package enpr.similarity;

import static org.junit.Assert.assertEquals;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cern.colt.function.DoubleFunction;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import enpr.PhyloProfTest;
import enpr.Profile;
import enpr.io.MatrixVectorReader;
import enpr.transform.Binary;
import enpr.transform.BinaryReverse;

public class SimilarityMeasureTest {
	static Profile profile;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Scanner in = new Scanner(new GZIPInputStream(PhyloProfTest.class.getResourceAsStream("/83333_realprof.csv.gz")));
		List<double[]> d = new ArrayList<double[]>();
		List<String> rowNames = new ArrayList<String>();
		List<String> colNames = new ArrayList<String>();
		String[] nextLine = null;
        while (in.hasNext()) {
        	String str = in.nextLine();
        	if (str.trim().isEmpty()) continue;
        	nextLine = str.split(",");
        	if (str.startsWith("#")) {
        		colNames.add(nextLine[2]);
        	} else {
        		String name = nextLine[0];
        		rowNames.add(name);
        		double[] line = new double[colNames.size()];
        		for (int n = 0; n < colNames.size(); n++) {
        			line[n] = Double.parseDouble(nextLine[n+1]);
        		}
        		d.add(line);
        	}
        }
        profile = new Profile();
        profile.setProfile(d.toArray(new double[d.size()][]));
		profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
		profile.setColNames(colNames.toArray(new String[colNames.size()]));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSimilarity() {
		JaccardIndex ji = new JaccardIndex();
		DoubleMatrix1D p1 = profile.getProfile().viewColumn(0);
		DoubleMatrix1D p2 = profile.getProfile().viewColumn(1);
		
		p1.assign(new DoubleFunction() {
			@Override
			public double apply(double arg0) {
				if (arg0 > 1e-5) return 0.0;
				else return 1.0;
			}
		});
		p2.assign(new DoubleFunction() {
			@Override
			public double apply(double arg0) {
				if (arg0 > 1e-5) return 0.0;
				else return 1.0;
			}
		});		
		double s = ji.similarity(p1.toArray(), p2.toArray());
		System.out.println(1-s);
	}

	@Test
	public void testJaccardIndexDoubleArrayDoubleArray() {
		JaccardIndex ji = new JaccardIndex();
		double s = ji.jaccardIndex(profile.getProfile().viewColumn(0).toArray(), profile.getProfile().viewColumn(1).toArray());
		System.out.println(s);		
	}

	@Test
	public void testJaccardIndexBitSetBitSet() {
		double[][] d = new double[][] {
				{1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0}, 
				{0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1},
				{1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1}
		};

		JaccardIndex j = new JaccardIndex();
		j.setMatrix(d);
		assertEquals(j.similarity(d[0], d[1]), j.similarity(0, 1), 0.000001);
		assertEquals(j.similarity(d[1], d[2]), j.similarity(1, 2), 0.000001);
		assertEquals(j.similarity(d[0], d[2]), j.similarity(0, 2), 0.000001);
	}
	
	@Test
	public void testHammingDistance() throws Exception {
		DoubleMatrix2D matrix = profile.getProfile().assign(new BinaryReverse(1e-7));
        HammingDistance h = new HammingDistance();
        String p1 = "P23878";
        String p2 = "P23886";
        int i1 = Arrays.asList(profile.getColNames()).indexOf(p1);
        int i2 = Arrays.asList(profile.getColNames()).indexOf(p2);
        assertEquals(0.0, h.similarity(matrix.viewRow(i1).toArray(), matrix.viewRow(i2).toArray()), 0.00000001);
	}
	
	@Test
	public void testHammingDistance2() throws Exception {
		DoubleMatrix2D matrix = profile.getProfile().assign(new BinaryReverse(1e-7));
        HammingDistance h = new HammingDistance();
        String p1 = "P37188";
        String p2 = "P0A6H5";
        int i1 = Arrays.asList(profile.getColNames()).indexOf(p1);
        int i2 = Arrays.asList(profile.getColNames()).indexOf(p2);
        assertEquals(2.0, h.similarity(matrix.viewRow(i1).toArray(), matrix.viewRow(i2).toArray()), 0.00000001);
	}	
	
	@Test
	public void testHammingDistance3() throws Exception {
		DoubleMatrix2D matrix = profile.getProfile().assign(new BinaryReverse(1e-7));
        HammingDistance h = new HammingDistance();

        Scanner in = new Scanner(this.getClass().getResourceAsStream("/83333_realprof_sb_results.csv"));
		while (in.hasNext()) {
			String[] t = in.nextLine().split(",");
	        int i1 = Arrays.asList(profile.getColNames()).indexOf(t[0]);
	        int i2 = Arrays.asList(profile.getColNames()).indexOf(t[1]);
			System.out.println(t[0] + ", " + t[1] + ", " + t[2] + ", " + (int)h.similarity(matrix.viewRow(i1).toArray(), matrix.viewRow(i2).toArray()));
			assertEquals((int)Double.parseDouble(t[2]), (int)h.similarity(matrix.viewRow(i1).toArray(), matrix.viewRow(i2).toArray()));
		}
	}	
	
	@Test
	public void testHypergeometricPValue() throws Exception {
		DoubleMatrix2D matrix = profile.getProfile().assign(new BinaryReverse(1e-7));
		HypergeometricPValue h = new HypergeometricPValue();
		
        Scanner in = new Scanner(this.getClass().getResourceAsStream("/83333_realprof_hg_results.csv"));
		while (in.hasNext()) {
			String[] t = in.nextLine().split(",");
	        int i1 = Arrays.asList(profile.getColNames()).indexOf(t[0]);
	        int i2 = Arrays.asList(profile.getColNames()).indexOf(t[1]);
			System.out.println(t[0] + ", " + t[1] + ", " + t[2] + ", " + (int)h.similarity(matrix.viewRow(i1).toArray(), matrix.viewRow(i2).toArray()));
			assertEquals((int)Double.parseDouble(t[2]), (int)h.similarity(matrix.viewRow(i1).toArray(), matrix.viewRow(i2).toArray()));
		}		
 	}	

	@Test
	public void testHypergeometricPValue2() throws Exception {
		HypergeometricPValue h = new HypergeometricPValue();
		
		MatrixVectorReader reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(PhyloProfTest.class.getResourceAsStream("/imgm_cog.prof.gz"))));
		List<String> rowNames = new ArrayList<>();
		List<String> colNames = new ArrayList<>();
		double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
		
		matrix = new DenseDoubleMatrix2D(matrix).assign(new Binary()).toArray();

        String p1 = "COG0020";
        String p2 = "COG1657";
        int i1 = rowNames.indexOf(p1);
        int i2 = rowNames.indexOf(p2);
        assertEquals(2.0, h.similarity(matrix[i1], matrix[i2]), 0.00000001);	
 	}		

	@Test
	public void testMutualInformation() throws Exception {
		double[] d1 = new double[] {1.9957476, 1.6440894, -1.2947007, -1.0414998, 1.5218424};
		double[] d2 = new double[] {-0.5645348, 0.1013099,  0.8396287, -0.7865699, 0.3027753};
		assertEquals(0.11849392256, new MutualInformation(3).similarity(d1, d2), 0.0000001);
	}
}
