package enpr.optimizer;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

import org.apache.mahout.common.Pair;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cern.colt.matrix.linalg.Algebra;
import ch.usi.inf.sape.hac.dendrogram.Dendrogram;
import enpr.Predictor;
import enpr.Profile;
import enpr.cluster.EnvironmentClusterer;
import enpr.io.MatrixVectorReader;
import enpr.io.PairLinkDataReader;
import enpr.io.newick.NewickParser;
import enpr.similarity.PearsonsCorrelation;
import enpr.similarity.SpearmansCorrelation;
import enpr.transform.NegativeLog;
import enpr.tree.DendrogramTree;
import enpr.tree.Tree;

public class OptimizerTest {
	static int[][] data;
	static int[] y;
	static Dendrogram tree;
	static Profile profile;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		profile = new Profile();
		List<String> colNames = new ArrayList<String>(); 
		List<String> rowNames = new ArrayList<String>();
		MatrixVectorReader reader = new MatrixVectorReader(new InputStreamReader(
				new GZIPInputStream(OptimizerTest.class.getResourceAsStream("/eco.prof.gz"))));
		double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
		profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
		profile.setColNames(colNames.toArray(new String[colNames.size()]));
		profile.setProfile(matrix);
		
		EnvironmentClusterer clusterer = new EnvironmentClusterer(profile);
		tree = clusterer.clusterDendrogram();
		
		profile = new Profile();
		reader = new MatrixVectorReader(new InputStreamReader(
				new GZIPInputStream(OptimizerTest.class.getResourceAsStream("/eco.log.prof.gz"))));
		profile.setProfile(reader.readDoubleMatrix2D());
		profile.setTree(new DendrogramTree(tree));
		
		List<int[]> list = new ArrayList<int[]>();
		List<Integer> yl = new ArrayList<Integer>();
		Scanner in = new Scanner(OptimizerTest.class.getResourceAsStream("/all.txt"));
		while (in.hasNext()) {
			String[] line = in.nextLine().split("\t");
			int[] row = new int[2];
			row[0] = Integer.parseInt(line[0]);
			row[1] = Integer.parseInt(line[1]);
			list.add(row);
			yl.add(Integer.parseInt(line[2]));
		}
		data = list.toArray(new int[list.size()][]);
		y = new int[yl.size()];
		for (int i = 0; i < yl.size(); i++) {
			y[i] = yl.get(i);
		}	
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testTreeBasedSearch() {
		Predictor predictor = new Predictor();
		TreeBasedSearch optimizer = new TreeBasedSearch(predictor, profile, data, y);
		optimizer.step1();
		optimizer.step2();
		
		for (int i = 0; i < optimizer.getCurrentSolution().size(); i++) {
			if (optimizer.getCurrentSolution().get(i) == 0.0) {
				System.out.println(i);
			}
		}
		System.out.println(optimizer.getAuc());				
		
		optimizer.step3();
		
		for (int i = 0; i < optimizer.getCurrentSolution().size(); i++) {
			if (optimizer.getCurrentSolution().get(i) == 0.0) {
				System.out.println(i);
			}
		}
		System.out.println(optimizer.getAuc());		
	}
	
	@Test
	public void testIterativeTaxonSelection() {
		Predictor predictor = new Predictor();
		IterativeTaxonSelection optimizer = new IterativeTaxonSelection(predictor, profile, data, y);
		boolean[] result = optimizer.optimize();
		for (int i = 0; i < result.length; i++) {
			if (!result[i]) System.out.println(i);
		}
		System.out.println(new Algebra().norm1(optimizer.getCurrentSolution()));
	}
	
	@Test
	public void testGenericAlgorithmOptimizer() {
		Predictor predictor = new Predictor();
		GeneticAlgorithmOptimizer optimizer = new GeneticAlgorithmOptimizer(predictor, profile, data, y);
		boolean[] result = optimizer.optimize();
		for (int i = 0; i < result.length; i++) {
			if (!result[i]) System.out.println(i);
		}
	}
	
	@Test
	public void testTreeBasedSearch2() throws Exception {
		profile = new Profile();
		List<String> colNames = new ArrayList<String>(); 
		List<String> rowNames = new ArrayList<String>();
		MatrixVectorReader reader = new MatrixVectorReader(new InputStreamReader(
				new GZIPInputStream(OptimizerTest.class.getResourceAsStream("/bsu/ep.prof.gz"))));
		double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
		profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
		//profile.setColNames(colNames.toArray(new String[colNames.size()]));
		profile.setProfile(matrix);
		profile.setProfile(profile.getProfile().assign(new NegativeLog()));
		
		Tree tree = new NewickParser(OptimizerTest.class.getResourceAsStream("/bsu/newick.txt")).tree();
		profile.setTree(tree);
		
		List<int[]> list = new ArrayList<int[]>();
		List<Integer> yl = new ArrayList<Integer>();
		Scanner in = new Scanner(new GZIPInputStream(OptimizerTest.class.getResourceAsStream("/bsu/pairs.txt.gz")));
		in.nextLine();
		while (in.hasNext()) {
			String[] line = in.nextLine().split("\t");
			int[] row = new int[2];
			row[0] = rowNames.indexOf(line[0]);
			row[1] = rowNames.indexOf(line[1]);
			list.add(row);
			yl.add(Integer.parseInt(line[2]));
		}
		data = list.toArray(new int[list.size()][]);
		y = new int[yl.size()];
		for (int i = 0; i < yl.size(); i++) {
			y[i] = yl.get(i);
		}			
		
		TreeBasedSearch optimizer = new TreeBasedSearch(new Predictor(), profile, data, y);
		optimizer.optimize();
		System.out.println(optimizer.getAuc());
	}
	
	@Test
	public void testTreeBasedSearch3() throws Exception {
		profile = new Profile();
		List<String> colNames = new ArrayList<String>(); 
		List<String> rowNames = new ArrayList<String>();
		MatrixVectorReader reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(OptimizerTest.class.getResourceAsStream("/imgm_cog.prof.gz"))));
		double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
		profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
		profile.setColNames(colNames.toArray(new String[colNames.size()]));
		profile.setProfile(matrix);
		
		Tree tree = new NewickParser(OptimizerTest.class.getResourceAsStream("/imgm_cog.newick")).tree();
		profile.setTree(tree);
		Pair<int[][], int[]> pairs = new PairLinkDataReader().readData(OptimizerTest.class.getResourceAsStream("/cog_pairs.txt"), profile.getRowNames());
		data = pairs.getFirst();
		y = pairs.getSecond();
		
		Predictor predictor = new Predictor();
		predictor.setMethod(new PearsonsCorrelation());
		
		TreeBasedSearch optimizer = new TreeBasedSearch(predictor, profile, data, y);
		optimizer.optimize();
		System.out.println(optimizer.getAuc());
	}	
	
	@Test
	public void testTreeBasedSearch4() throws Exception {
		profile = new Profile();
		List<String> colNames = new ArrayList<String>(); 
		List<String> rowNames = new ArrayList<String>();
		MatrixVectorReader reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(OptimizerTest.class.getResourceAsStream("/imgm_cog.prof.gz"))));
		double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
		profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
		profile.setColNames(colNames.toArray(new String[colNames.size()]));
		profile.setProfile(matrix);
		
		Tree tree = new NewickParser(OptimizerTest.class.getResourceAsStream("/imgm_cog.newick")).tree();
		profile.setTree(tree);
		Pair<int[][], int[]> pairs = new PairLinkDataReader().readData(OptimizerTest.class.getResourceAsStream("/cog_pairs.txt"), profile.getRowNames());
		data = pairs.getFirst();
		y = pairs.getSecond();
		
		Predictor predictor = new Predictor();
		predictor.setMethod(new SpearmansCorrelation());
		
		TreeBasedSearch optimizer = new TreeBasedSearch(predictor, profile, data, y);
		optimizer.optimize();
		System.out.println(optimizer.getAuc());
	}	
	
}
