package enpr.dist;

import cern.colt.function.IntIntDoubleFunction;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.doublealgo.Statistic;
import cern.colt.matrix.linalg.Algebra;
import enpr.Profile;
import enpr.cluster.EnvironmentClusterer;
import enpr.io.MatrixVectorReader;
import enpr.similarity.MutualInformation;
import enpr.transform.NegativeLog;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class ChunkThreadedDistanceTest {
    static Profile profile;

    @BeforeClass
    public static void setupBeforeClass() throws IOException {
        profile = new Profile();
        List<String> colNames = new ArrayList<>();
        List<String> rowNames = new ArrayList<>();
        MatrixVectorReader reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(EnvironmentClusterer.class.getResourceAsStream("/imgm.prof.txt.gz"))));
        double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
        profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
        profile.setColNames(colNames.toArray(new String[colNames.size()]));
        profile.setProfile(matrix);

        System.out.println(rowNames.size());

        profile.setProfile(profile.getProfile().assign(new NegativeLog()));

    }

    @Test
    public void testDistanceSparse() throws Exception {
        final DoubleMatrix2D mat = new Algebra().subMatrix(profile.getProfile(), 0, 1925, 0, profile.getColNames().length-1);

        ChunkThreadedDistance d = new ChunkThreadedDistance(200, 4);

        long start = System.currentTimeMillis();
        DoubleMatrix2D r1 = d.distance(mat, new MutualInformation(), 0.1, false);
        long end = System.currentTimeMillis();
        System.out.println("With threads: " + ( end - start ) + " mS");

        MutualInformation mi = new MutualInformation();
        start = System.currentTimeMillis();
        DoubleMatrix2D r2 = Statistic.distance(mat.viewDice(), new MutualInformation());
        end = System.currentTimeMillis();
        System.out.println("W/O  threads: " + ( end - start ) + " mS");

        r1.forEachNonZero(new IntIntDoubleFunction() {
            @Override
            public double apply(int i1, int i2, double v) {
                if (i1 == i2) return 0.0;
                MutualInformation mi = new MutualInformation();
                double expected = mi.apply(mat.viewRow(i1), mat.viewRow(i2));
                Assert.assertEquals(expected, v, 1e-8);
                return 0.0;
            }
        });

    }
    @Test
    public void testWriteDistanceSparse() throws Exception {
        final DoubleMatrix2D mat = new Algebra().subMatrix(profile.getProfile(), 0, 1925, 0, profile.getColNames().length-1);

        ChunkThreadedDistance d = new ChunkThreadedDistance(200, 4);

        long start = System.currentTimeMillis();
        d.write(mat, new MutualInformation(), 0.1, false, new OutputStreamWriter(System.out));
        long end = System.currentTimeMillis();
        System.out.println("With threads: " + ( end - start ) + " mS");

        MutualInformation mi = new MutualInformation();
        start = System.currentTimeMillis();
        DoubleMatrix2D r2 = Statistic.distance(mat.viewDice(), new MutualInformation());
        end = System.currentTimeMillis();
        System.out.println("W/O  threads: " + ( end - start ) + " mS");
    }
}
