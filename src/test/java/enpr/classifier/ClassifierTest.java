package enpr.classifier;

import org.junit.*;
import weka.classifiers.multilabel.Evaluation;
import weka.classifiers.multilabel.MultilabelClassifier;
import weka.classifiers.multilabel.PS;
import weka.classifiers.multilabel.meta.EnsembleML;
import weka.core.Instances;
import weka.core.Result;
import weka.core.converters.ConverterUtils;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class ClassifierTest {
    @Test
    public void saveModelTest() throws Exception {
        String[] args = "-t C:/pkg/meka-1.3/data/Music.arff -x 10 -f EPR/result.out -W weka.classifiers.multilabel.PS -- -P 1-5 -N 1 -W weka.classifiers.functions.SMO".split(" ");
        MultilabelClassifier.runClassifier(new EnsembleML(), args);
    }
}
