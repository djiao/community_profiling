package enpr;

import static org.junit.Assert.*;

import org.apache.mahout.classifier.evaluation.Auc;
import org.junit.Test;

public class PredictorTest {

	@Test
	public void testAUC() {
		Auc auc = new Auc();
		int[] y = { 1, 0, 1, 0, 1, 1, 0, 0, 1, 0};
		double[] p = {0.8, 0.2, 0.56, 0.61, 0.4, 0.9, 0.3, 0.2, 0.5, 0.8};
		for (int i = 0; i < y.length; i++) {
			auc.add(y[i], p[i]);
		}
		assertEquals(auc.auc(), new Predictor().auc(y, p), 0.000001);
	}
}
