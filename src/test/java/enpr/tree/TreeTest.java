package enpr.tree;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import enpr.io.newick.NewickParser;

public class TreeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testToNewick() throws Exception {
		String newick = "((a,b)A,(c,d,(e,f)B)C)D;";
		Tree t = new NewickParser(new ByteArrayInputStream(newick.getBytes())).tree();
		assertEquals(newick, t.toNewick().replaceAll(" ", ""));
	}

	@Test
	public void testTrimTree() throws Exception {
		Tree t = new NewickParser(TreeTest.class.getResourceAsStream("/ncbi_complete_with_taxIDs.newick")).tree();
		
		Scanner s= new Scanner(TreeTest.class.getResourceAsStream("/kegg2taxonomy.txt"));
		List<String> ids = new ArrayList<String>();
		while (s.hasNext()) {
			ids.add(s.nextLine().split("\t")[1]);
		}
		
		t.mapLeafKeys(ids);
		
		t.trimTree();
		assertEquals(1, t.getLeafCount());
		
	}
}
