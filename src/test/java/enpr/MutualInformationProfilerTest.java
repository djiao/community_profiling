package enpr;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

import org.junit.Test;

import enpr.io.MatrixVectorReader;
import enpr.similarity.MutualInformation;

import junit.framework.TestCase;

public class MutualInformationProfilerTest extends TestCase {
	double[][] profile;
	List<String> colNames;
	List<String> rowNames;
	int[][] data;
	int[] y;
	
	public MutualInformationProfilerTest() throws IOException {
//		Scanner in = new Scanner(new GZIPInputStream(this.getClass().getResourceAsStream("/eco.log.prof.gz")));
//        String[] nextLine;
//        List<double[]> d = new ArrayList<double[]>();
//        while (in.hasNext()) {
//        	nextLine = in.nextLine().split("\t");
//            double[] line = new double[nextLine.length];
//            for (int i = 0; i < nextLine.length; i++) {
//                line[i] = Double.parseDouble(nextLine[i]);
//            }
//            d.add(line);
//        } 
		MatrixVectorReader reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(this.getClass().getResourceAsStream("/eco.log.prof.gz"))));
        profile = reader.readDoubleMatrix2D();
		
		List<int[]> list = new ArrayList<int[]>();
		List<Integer> yl = new ArrayList<Integer>();
		Scanner in = new Scanner(this.getClass().getResourceAsStream("/all.txt"));
		while (in.hasNext()) {
			String[] line = in.nextLine().split("\t");
			int[] row = new int[2];
			row[0] = Integer.parseInt(line[0]);
			row[1] = Integer.parseInt(line[1]);
			list.add(row);
			yl.add(Integer.parseInt(line[2]));
		}
		data = list.toArray(new int[list.size()][]);
		y = new int[yl.size()];
		for (int i = 0; i < yl.size(); i++) {
			y[i] = yl.get(i);
		}	
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	@Test
	public void testAuc() {
		Predictor p = new Predictor();
		p.setMethod(new MutualInformation(10));
		double[] value = p.similarity(profile, data);
		System.out.println(p.auc(y, value));
	}

}
