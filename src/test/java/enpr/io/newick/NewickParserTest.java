package enpr.io.newick;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import enpr.tree.Tree;

public class NewickParserTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNewickParser() throws Exception {
		String nw = "((ar, tk) 23 , (abc, ((xa, xb) 26 , ((lima, limb) 28 , (((spi, sfi) 31 , (eqc, (kme, ame) 33 ) 32 ) 30 , ((fxf, (tva, tvb) 36 ) 35 , (fxe, (cjk, ((mrb, (qq, rr) 41 ) 40 , sgn) 39 ) 38 ) 37 ) 34 ) 29 ) 27 ) 25 ) 24 ) 22 ;";
		NewickParser parser = new NewickParser(new ByteArrayInputStream(nw.getBytes()));
		Tree tree = parser.tree();
		assertEquals(21, tree.getLeafCount());
	}
	
	@Test
	public void testNewickParser3() throws Exception {
		String nw = "((123, 124:-0.112) 456, (1, 2) 678: 0.33) 900 ;";
		NewickParser parser = new NewickParser(new ByteArrayInputStream(nw.getBytes()));
		Tree tree = parser.tree();
		assertEquals(4, tree.getLeafCount());		
	}
	
	@Test
	public void testNewickParser2() throws Exception {
		NewickParser parser = new NewickParser(NewickParserTest.class.getResourceAsStream("/taxomonicTreeNCBI_bin.nwk"));
		Tree tree = parser.tree();
		assertEquals(980, tree.getLeafCount());
		assertEquals(1940, tree.getTotalNodeCount());
		System.out.println(tree.getHeight());
	}
}
