package enpr.cluster;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.usi.inf.sape.hac.dendrogram.Dendrogram;
import enpr.Profile;
import enpr.io.MatrixVectorReader;
import enpr.transform.BinaryReverse;
import enpr.tree.Tree;

public class EnvironmentClustererTest {
	static Profile profile;

	@BeforeClass
	public static void setupBeforeClass() throws IOException {

	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCluster1() throws Exception {
		profile = new Profile();
		List<String> colNames = new ArrayList<>();
		List<String> rowNames = new ArrayList<>();
		MatrixVectorReader reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(EnvironmentClusterer.class.getResourceAsStream("/imgm.prof.txt.gz"))));
		double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
		profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
		profile.setColNames(colNames.toArray(new String[colNames.size()]));
		profile.setProfile(matrix);		
		
		profile.setProfile(profile.getProfile().assign(new BinaryReverse(1e-5)));
		EnvironmentClusterer clusterer = new EnvironmentClusterer(profile);
		int[][] cm = clusterer.clusterMatrix();
		
		
		Dendrogram d = clusterer.clusterDendrogram();
		System.out.println(d);
		
		Tree tree = clusterer.clusterTree();
		Assert.assertEquals(304, tree.getLeafCount());
	}
	
	@Test
	public void testCluster2() throws Exception {
		profile = new Profile();
		List<String> colNames = new ArrayList<String>(); 
		List<String> rowNames = new ArrayList<String>();
		MatrixVectorReader reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(EnvironmentClusterer.class.getResourceAsStream("/bsu/ep.prof.gz"))));
		double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
		profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
		profile.setColNames(colNames.toArray(new String[colNames.size()]));
		profile.setProfile(matrix);		
		
		profile.setProfile(profile.getProfile().assign(new BinaryReverse(1e-5)));
		EnvironmentClusterer clusterer = new EnvironmentClusterer(profile);
		int[][] cm = clusterer.clusterMatrix();
			
		Dendrogram d = clusterer.clusterDendrogram();
		System.out.println(d);
		
		Tree tree = clusterer.clusterTree();
		Assert.assertEquals(994, tree.getLeafCount());
	}	
}
