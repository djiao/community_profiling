package enpr;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.primitives.Ints;

import enpr.io.newick.NewickParser;
import enpr.optimizer.GeneticAlgorithmOptimizer;
import enpr.optimizer.TreeBasedSearch;
import enpr.similarity.HammingDistance;
import enpr.similarity.HypergeometricPValue;
import enpr.transform.BinaryReverse;
import enpr.transform.NegativeInverseLog;
import enpr.tree.Tree;
import enpr.tree.TreeNode;

public class PhyloProfTest {
	
	static Profile profile;
	static int[][] data;
	static int[] y;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Scanner in = new Scanner(new GZIPInputStream(PhyloProfTest.class.getResourceAsStream("/83333_realprof.csv.gz")));
		List<double[]> d = new ArrayList<double[]>();
		List<String> rowNames = new ArrayList<String>();
		List<String> colNames = new ArrayList<String>();
		String[] nextLine = null;
        while (in.hasNext()) {
        	String str = in.nextLine();
        	if (str.trim().isEmpty()) continue;
        	nextLine = str.split(",");
        	if (str.startsWith("#")) {
        		colNames.add(nextLine[2]);
        	} else {
        		String name = nextLine[0];
        		rowNames.add(name);
        		double[] line = new double[colNames.size()];
        		for (int n = 0; n < colNames.size(); n++) {
        			line[n] = Double.parseDouble(nextLine[n+1]);
        		}
        		d.add(line);
        	}
        }
        profile = new Profile();
        profile.setProfile(d.toArray(new double[d.size()][]));
        profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
        profile.setColNames(colNames.toArray(new String[colNames.size()]));
        
        Tree tree = new NewickParser(PhyloProfTest.class.getResourceAsStream("/taxomonicTreeNCBI_bin.nwk")).tree();
        for (TreeNode leaf: tree.getRoot().getLeaves()) {
        	leaf.setLeafKey(colNames.indexOf(leaf.getName().split("@")[1]));
        	if (leaf.getLeafKey() < 0) {
        		System.out.println(leaf.getName());
        	}
        }
        profile.setTree(tree);
        
        in.close();
        
        in = new Scanner(PhyloProfTest.class.getResourceAsStream("/83333_proteinPairsWithLink.txt"));
        List<int[]> datal = new ArrayList<int[]>();
        List<Integer> yl = new ArrayList<Integer>();
        while (in.hasNext()) {
        	String[] tokens = in.nextLine().split("\t");
        	int[] r = new int[2];
        	r[0] = rowNames.indexOf(tokens[0]);
        	r[1] = rowNames.indexOf(tokens[1]);
        	datal.add(r);
        	yl.add(Integer.parseInt(tokens[2]));
        }
        data = datal.toArray(new int[datal.size()][]);
        y = Ints.toArray(yl);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testTreeBasedSearch() {
        profile.setProfile(profile.getProfile().assign(new NegativeInverseLog()));
		Predictor predictor = new Predictor();
		System.out.println(predictor.auc(y, predictor.similarity(profile.getProfile().toArray(), data)));
		TreeBasedSearch tbs = new TreeBasedSearch(predictor, profile, data, y);
		boolean[] result = tbs.optimize();
		for (int i = 0; i < result.length; i++) {
			if (!result[i]) System.out.println(i);
		}
		System.out.println(tbs.getAuc());
	}
	
	@Test
	public void testGeneticAlgorithm() {
        profile.setProfile(profile.getProfile().assign(new NegativeInverseLog()));
		Predictor predictor = new Predictor();
		GeneticAlgorithmOptimizer optimizer = new GeneticAlgorithmOptimizer(predictor, profile, data, y);
		boolean[] result = optimizer.optimize();
		for (int i = 0; i < result.length; i++) {
			if (!result[i]) System.out.println(i);
		}		
	}
	
	@Test
	public void testPellegrini() {
        profile.setProfile(profile.getProfile().assign(new BinaryReverse(1e-7)));
        Predictor predictor = new Predictor();
        predictor.setMethod(new HammingDistance());
        System.out.println(predictor.auc(y, predictor.similarity(profile.getProfile().toArray(), data)));
		TreeBasedSearch tbs = new TreeBasedSearch(predictor, profile, data, y);
		boolean[] result = tbs.optimize();
		for (int i = 0; i < result.length; i++) {
			if (!result[i]) System.out.println(i);
		}
		System.out.println(tbs.getAuc());        
	}
	
	@Test
	public void testHypergeometric() {
        profile.setProfile(profile.getProfile().assign(new BinaryReverse(1e-7)));
        Predictor predictor = new Predictor();
        predictor.setMethod(new HypergeometricPValue());
        System.out.println(predictor.auc(y, predictor.similarity(profile.getProfile().toArray(), data)));
		TreeBasedSearch tbs = new TreeBasedSearch(predictor, profile, data, y);
		boolean[] result = tbs.optimize();
		for (int i = 0; i < result.length; i++) {
			if (!result[i]) System.out.print(i + ' ');
		}
		System.out.println();
		System.out.println(tbs.getAuc());   		
	}
	
}
