package enpr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PatternOptionBuilder;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.io.FileUtils;
import org.apache.mahout.common.Pair;

import cern.colt.function.DoubleFunction;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import enpr.io.MatrixVectorReader;
import enpr.io.PairLinkDataReader;
import enpr.similarity.SimilarityMeasure;
import enpr.transform.BinaryReverse;

/**
 * @author djiao
 *
 */
public class App {
	private static final Logger logger = Logger.getLogger(App.class.getName());
	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		CommandLineParser parser = new PosixParser();
		File profFile = null;
		File dataFile = null;
		File outFile = null;
		DoubleFunction transformer = null;
		double cutoff = 1e-5;
		Class<DoubleFunction> transformerClass = null;
		Class<SimilarityMeasure> similarityClass = null;
		File listFile = null;
		String columnName = null;

		Options options = new Options();
		options.addOption(App.buildOption("h", "help",        false, "print help", false, null));
		options.addOption(App.buildOption("d", "data-file",   true, "functional links data (required)", true, PatternOptionBuilder.EXISTING_FILE_VALUE));
		options.addOption(App.buildOption("p", "profile",     true, "profile matrix (required)", true, PatternOptionBuilder.EXISTING_FILE_VALUE));
		options.addOption(App.buildOption("r", "transformer", true, "value transformer (optional, default null)", false, PatternOptionBuilder.CLASS_VALUE));
		options.addOption(App.buildOption("c", "cutoff",      true, "cutoff value if transformer is enpr.transform.BinaryReverse. (optional, default 1e-5)", false, PatternOptionBuilder.NUMBER_VALUE));
		options.addOption(App.buildOption("o", "output",      true, "output file (optional, default: data-file, result append after the last column)", false, PatternOptionBuilder.FILE_VALUE));
		options.addOption(App.buildOption("s", "similarity",  true, "similarity measure class (optional, default enpr.similarity.MutualInformation", false, PatternOptionBuilder.CLASS_VALUE));
		options.addOption(App.buildOption("l", "sample-list-file", true, "optimized list of samples/columns in profile (optional, default every column)", false, PatternOptionBuilder.EXISTING_FILE_VALUE));				
		options.addOption(App.buildOption("n", "column-name", true, "name of the new column (required)", true, PatternOptionBuilder.STRING_VALUE));				
		
		try {
			CommandLine line = parser.parse(options, args);
			if (line.hasOption('h')) {
				App.printHelp(options, TreeBuilder.class.getName());
				System.exit(0);
			}
			
			profFile = (File)line.getParsedOptionValue("p");
			dataFile = (File)line.getParsedOptionValue("d");
			if (line.hasOption('o'))
				outFile = (File)line.getParsedOptionValue("o");

			columnName = (String) line.getParsedOptionValue("n");
			
			if (line.hasOption('r')) {
				if (line.hasOption('c')) {
					cutoff = ((Number)line.getParsedOptionValue("c")).doubleValue();
				}
				transformerClass = (Class<DoubleFunction>)line.getParsedOptionValue("r");
				if (transformerClass.getName().equals(BinaryReverse.class.getName())) {
					transformer = new BinaryReverse(cutoff);
				} else {
					transformer = (DoubleFunction)transformerClass.newInstance();
				}
			} 
			
			if (line.hasOption('s')) {
				similarityClass = (Class<SimilarityMeasure>)line.getParsedOptionValue("s");
			} 
			
			if (line.hasOption('l')) {
				listFile = (File)line.getParsedOptionValue("l");
			}
		} catch (ParseException ex) {
			logger.log(Level.SEVERE, "Unexpected exception: {0}", ex.getMessage());
			printHelp(options, App.class.getName());
			System.exit(1);
		} catch (InstantiationException e) {
			logger.log(Level.SEVERE, "Can't start transformer class {0}: {1}", new Object[]{transformerClass.getName(), e.getMessage()});
			System.exit(1);
		} catch (IllegalAccessException e) {
			logger.log(Level.SEVERE, "Can't start transformer class {0}: {1}", new Object[]{transformerClass.getName(), e.getMessage()});
			System.exit(1);
		}
		
		MatrixVectorReader reader = null;
		Profile profile = new Profile();
		List<String> colNames = new ArrayList<String>(); 
		List<String> rowNames = new ArrayList<String>();
		PrintWriter writer = null;
		try {
			if (profFile.getName().endsWith("gz")) {
				reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(profFile))));
			} else if (profFile.getName().endsWith("zip")) {
				reader = new MatrixVectorReader(new InputStreamReader(new ZipInputStream(new FileInputStream(profFile))));
			} else {
				reader = new MatrixVectorReader(new FileReader(profFile));
			}
			double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
			profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
			profile.setColNames(colNames.toArray(new String[colNames.size()]));
			profile.setProfile(matrix);	
			if (listFile != null) {
				List<String> lines = Files.readLines(listFile, Charsets.UTF_8);
				String[] cols = lines.toArray(new String[lines.size()]);
				profile.selectColumns(cols);
			}			
			if (transformer != null) {
				profile.setProfile(profile.getProfile().assign(transformer));
			}
			
			Pair<int[][], int[]> pair = null;
			PairLinkDataReader linkReader = new PairLinkDataReader();
			if (dataFile.getName().endsWith("gz")) {
				pair = linkReader.readData(new GZIPInputStream(new FileInputStream(dataFile)), profile.getRowNames());
			} else if (dataFile.getName().endsWith("zip")) {
				pair = linkReader.readData(new ZipInputStream(new FileInputStream(dataFile)), profile.getRowNames());
			} else {
				pair = linkReader.readData(new FileInputStream(dataFile), profile.getRowNames());
			}		
			
			Predictor predictor = new Predictor();
			if (similarityClass != null) {
				predictor.setMethod(similarityClass.newInstance());
			}
			
			double[] similarity = predictor.similarity(profile.getProfile().toArray(), pair.getFirst());
			boolean tempout = (outFile == null);
			if (outFile == null) { 
				outFile = File.createTempFile("enpr_temp", ".txt");
				outFile.deleteOnExit();
			}
			writer = new PrintWriter(new FileWriter(outFile));
			Scanner scanner = new Scanner(dataFile);
			int i = 0; 
			writer.write(scanner.nextLine().trim() + "\t" + columnName + "\n"); // header
			while (scanner.hasNext()) {
				writer.write(scanner.nextLine().trim() + "\t" + String.format("%12e", similarity[i++]) + "\n");
			}
			writer.flush();
			writer.close();
			scanner.close();
			if (tempout) {
				FileUtils.copyFile(outFile, dataFile);
			}
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Error in reading file {0}", ex.getMessage());
			System.exit(1);
		} catch (InstantiationException e) {
			logger.log(Level.SEVERE, "Can't start SimilarityMeasure class {0}: {1}", new Object[]{similarityClass.getName(), e.getMessage()});
			System.exit(1);
		} catch (IllegalAccessException e) {
			logger.log(Level.SEVERE, "Can't start SimilarityMeasure class {0}: {1}", new Object[]{similarityClass.getName(), e.getMessage()});
			System.exit(1);
		}
	}

	public static void printHelp(Options options, String cls) {
		HelpFormatter hf = new HelpFormatter();
		hf.printHelp("java " + cls, options);
	}

	@SuppressWarnings("static-access")
	public static Option buildOption(String opt, String longOpt, boolean hasArg, String description,
			boolean isRequired, Object type) {
		return OptionBuilder.withLongOpt(longOpt).hasArg(hasArg).withDescription(description).isRequired(isRequired)
				.withType(type).create(opt);
	}	
}
