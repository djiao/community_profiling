package enpr.transform;

import cern.colt.function.DoubleFunction;

public class NegativeLog implements DoubleFunction {

	@Override
	public double apply(double arg0) {
		  //profile[profile > 1] = 1 # avoid negative log values
			//	  profile[profile == 0] = min(profile[profile > 0]) * 0.1 # avoid inf values
				//  log.prof = -log10(profile)		
		arg0 = arg0 > 0.0 ? arg0 : 1e-180;  // min evalue is 1e-180. 
		return -Math.log10(Math.min(1.0, arg0));
	}
}
