package enpr.transform;

import cern.colt.function.DoubleFunction;

/**
 * Returns 1.0 if d is greater than cutoff, otherwise returns 0.0
 * 
 * @author djiao
 */
public class Binary implements DoubleFunction {
	private double cutoff = 0.0;
	public Binary() { super(); }
	public Binary(double cutoff) { this.cutoff = cutoff; }
	@Override
	public double apply(double arg0) {
		return arg0 > cutoff ? 1.0 : 0.0;
	}
}
