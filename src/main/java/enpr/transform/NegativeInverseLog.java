package enpr.transform;

import cern.colt.function.DoubleFunction;

/**
 * When d >= 0.1, returns 1.0. Otherwise, 
 * returns -1/log_10{d} (negative inverse log) 
 * 
 * @author djiao
 */
public class NegativeInverseLog implements DoubleFunction {
	@Override
	public double apply(double arg0) {
		return -1.0/Math.log10(Math.min(0.1, arg0));
	}
}
