package enpr.transform;

import cern.colt.function.DoubleFunction;

public class BinaryReverse implements DoubleFunction {
	private double cutoff;
	public BinaryReverse(double cutoff) {
		this.cutoff = cutoff;
	}
	@Override
	public double apply(double arg0) {
		return arg0 > cutoff ? 0.0 : 1.0;
	}

}
