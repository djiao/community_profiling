package enpr.tree;

import ch.usi.inf.sape.hac.dendrogram.Dendrogram;

public class DendrogramTree extends Tree {
	private Dendrogram dendrogram;
	
	public DendrogramTree(Dendrogram dendrogram) {
		this.dendrogram = dendrogram;
		this.root = new DendrogramTreeNode(dendrogram.getRoot());
	}

	/**
	 * @return the dendrogram
	 */
	public Dendrogram getDendrogram() {
		return dendrogram;
	}

	/**
	 * @param dendrogram the dendrogram to set
	 */
	public void setDendrogram(Dendrogram dendrogram) {
		this.dendrogram = dendrogram;
	}
	
	
}
