package enpr.tree;

import ch.usi.inf.sape.hac.dendrogram.DendrogramNode;
import ch.usi.inf.sape.hac.dendrogram.ObservationNode;

public class DendrogramTreeNode extends TreeNode {
	public DendrogramTreeNode(DendrogramNode node) {
		super();
		if (node.getObservationCount() > 1) {
			this.addChild(new DendrogramTreeNode(node.getLeft()));
			this.addChild(new DendrogramTreeNode(node.getRight()));
		} else {
			this.leafKey = ((ObservationNode)node).getObservation();
		}
	}
}
