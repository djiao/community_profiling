package enpr.tree;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author djiao
 * @see Tree
 */
public class TreeNode {

	protected List<TreeNode> children; 
	
	protected List<TreeNode> leaves;

	protected int key;
	
	protected int numOfDescendants;
	
	/**
	 * If a leaf node, this key is the index that corresponds to the position 
	 * of the leaf in the original order of leaves
	 */
	protected int leafKey = -1;

	/** The parent of this node.  This is null for the root node. */
	protected TreeNode parent;

	/**
	 * Node name with default "". Most internal nodes have no name and all leaf
	 * nodes have a name.  This becomes the long version of the node name when fully
	 * qualified names are used.
	 */
	protected String name = ""; // the long form in fully qualified names

	/** Distance from this node to the root node. The root is at height 0. */
	protected int height = 0;

	/**
	 * Leaves all have the same level. Internal nodes's levels are the min value of all
	 * children's levels minus 1. 
	 */
	protected int level = Integer.MAX_VALUE;
	
	/** Weight is the horizontal edge length for the edge immediately above the node.  Edge lengths are not determined by this number currently; all edges are stretched to make leaves right aligned, with minimal integral lengths. */
	protected double weight = 0.0d;

	/**
	 * Returns the key for this node.
	 * @return The value of {@link #key} for this node.
	 */
	public int getKey() {
		return key;
	}

	/**
	 * Returns the label for this node, which is {@link #name}.
	 * @return The value of {@link #name} for this node.
	 */
	public String getName() {
		return name;
	}

	/** Implements Comparable interface - sorts on key field. 
	 * @param o The other object to compare this node to.
	 * @return -1 if this is smaller than the object's key, +1 if greater, 0 if equal. */
	public int compareTo(Object o) {
		if (key == ((TreeNode) o).key)
			return 0;
		else if (key < ((TreeNode) o).key)
			return -1;
		else
			return 1;
	}


	public TreeNode() {
		children = new ArrayList<TreeNode>();
		leaves = new ArrayList<TreeNode>();
	}

	public void setName(String s) {
		name = s;
	}

	public int numberChildren() {
		return children.size();
	}

	public TreeNode getChild(int i) {
		if (i < children.size())
			return (TreeNode) children.get(i);
		else
			return null;
	}

	public boolean isLeaf() {
		return children.isEmpty();
	}

	public boolean isRoot() {
		return (null == parent);
	}

	public boolean equals(TreeNode n) {
		return (name.equals(n.name));
	}

	public void addChild(TreeNode n) {
		children.add(n);
		n.parent = this;
	}

	public TreeNode getParent() {
		return parent;
	}
	
	public void setParent(TreeNode n) {
		this.parent = n;
	}

	/**
	 * Set the weight of this treenode, which encodes the length of the horizontal edge.
	 * Edge weights are not implemented currently for drawing.
	 * @param w New edge weight for this node, {@link #weight}.
	 */
	public void setWeight(double w) {
		weight = (float) w;
	}

	/**
	 * Get the weight of this treenode, which encodes the length of the horizontal edge.
	 * Edge weights are not implemented currently for drawing.
	 * @return Edge weight for this node, {@link #weight}.
	 */
	public double getWeight() {
		return weight;
	}

	/** Get the first child of this node. Doesn't work with leaf nodes.
	 * @return First child of this internal node.
	 */
	protected TreeNode firstChild() {
		return (TreeNode) children.get(0);
	}

	/** Get the last child of this node. Doesn't work with leaf nodes.
	 * @return Last child of this internal node.
	 */
	public TreeNode lastChild() {
		return (TreeNode) children.get(children.size() - 1);
	}

	/**
	 * Long form printing for a single node. Used in conjunction with
	 * {@link #printSubtree()} to display a whole subtree.
	 * 
	 */
	public void print() {
		if (name != null)
			System.out.print("node name: " + name + "\t");
		else
			System.out.print("node name null,\t");
		System.out.println("key: " + key);
	}

	public int getNumberLeaves() {
		return getLeaves().size();
	}
	
	public List<TreeNode> getLeaves() {
		if (this.leaves.isEmpty()) {
			if (isLeaf()) {
				leaves.add(this);
			} else {
				for (TreeNode child : children) {
					leaves.addAll(child.getLeaves());
				}
			}
		}
		return this.leaves;
	}
	
	public List<TreeNode> resetLeaves() {
		this.leaves.clear();
		if (isLeaf()) {
			leaves.add(this);
		} else {
			for (TreeNode child : children) {
				leaves.addAll(child.resetLeaves());
			}
		}		
		return this.leaves;
	}

	/**
	 * String value of this node, name + key + tree height information.
	 * @return String representation of this node.
	 */
	public String toString() {
		// String edge[] = {edges[0]!=null?edges[0].toString():"X",
		// edges[1]!=null?edges[1].toString():"Y"};
		return name + "(" + key + " @ " + height + ")";
	}

	/**
	 * @return the children
	 */
	public List<TreeNode> getChildren() {
		return children;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		if (isRoot()) return 0;
		else if (this.height == 0) { 
			this.height = parent.getHeight() + 1;
		}
		return this.height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @return the leafKey
	 */
	public int getLeafKey() {
		return leafKey;
	}

	/**
	 * @param leafKey the leafKey to set
	 */
	public void setLeafKey(int leafKey) {
		this.leafKey = leafKey;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(int key) {
		this.key = key;
	}
	
	public int getNumOfDescendants() {
		if (numOfDescendants == 0 && !children.isEmpty()) {
			for (TreeNode child: children) {
				numOfDescendants += (child.getNumOfDescendants() + 1);
			}
		}
		return numOfDescendants;
	}
	
	public int getLevel() {
		if (level == Integer.MAX_VALUE) {
			for (TreeNode child: children) {
				level = Math.min(level, child.getLevel());
			}
		}
		return level;
	}
	
	public String toNewick() {
		if (this.isLeaf()) {
			return escapeName(name);
		} else {
			List<String> c = new ArrayList<String>();
			for (TreeNode child: getChildren()) {
				c.add(child.toNewick());
			}
			return "(" + StringUtils.join(c, ",") + ")" + escapeName(name);
		}
	}
	
	public static String escapeName(String name) {
		if (name.contains(":") || name.contains("'")) {
			return "\"" + name + "\"";
		} else if (name.contains("\"")) {
			return "'" + name + "'";
		} else {
			return name;
		}
	}
}