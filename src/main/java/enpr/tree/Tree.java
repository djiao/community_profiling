package enpr.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

/**
 * A tree structure
 * 
 * @author djiao
 * @see TreeNode
 */

public class Tree {
	private final static Logger log = Logger.getLogger(Tree.class.getName()); 

	private Map<String, TreeNode> nodesByName;
	private int numOfNodes;
	
	private Map<String, String[]> leafProperties = new HashMap<String, String[]>();
	
	
	public Tree() {
		root = new TreeNode();
		nodesByName = new HashMap<String, TreeNode>();
	}

	public Tree(Tree treeToCopy) {
		height = treeToCopy.height;
		nodesByName = new HashMap<String, TreeNode>(treeToCopy.nodesByName);
		root = treeToCopy.root;
	}

	/**
	 * Returns the number of internal nodes in this tree. For debugging.
	 * 
	 * @return Total number of nodes minus the number of leaves.
	 */
	public int getInternalNodeCount() {
		return getTotalNodeCount() - getRoot().getNumberLeaves();
	}

	/**
	 * Returns the node count, for internal and leaf nodes.
	 * 
	 * @return Size of the {@link #nodes} array, which contains all nodes.
	 */
	public int getTotalNodeCount() {
		if (numOfNodes == 0) {
			numOfNodes = root.getNumOfDescendants() + 1;
		}
		return numOfNodes;
	}

	/**
	 * Returns the node given by the string.
	 * 
	 * @param s
	 *            Name/label of node to retrieve.
	 * @return Treenode referenced by the given name.
	 */
	public TreeNode getNodeByName(String s) {
		return (TreeNode) nodesByName.get(s);
	}

	/**
	 * Height of tree, which is also the longest path from the root to some leaf
	 * node.
	 */
	private int height = 0;

	/**
	 * Accessor for height of tree. This is also the longest path from the root
	 * to some leaf node.
	 * 
	 * @return value of {@link #height}.
	 */
	public int getHeight() {
		if (height == 0) {
			for (TreeNode leaf: this.getRoot().getLeaves()) {
				height = Math.max(height, leaf.getHeight());
			}
		}
		return height;
	}


	/**
	 * Root accessor.
	 * 
	 * @return Value of {@link #root}
	 */
	public TreeNode getRoot() {
		return root;
	}
	
	public void setRoot(TreeNode root) {
		this.root = root;
	}

	public void setRootNode(TreeNode newRoot) {
		this.root = newRoot;
	}

	/**
	 * Root node of this tree
	 */
	protected TreeNode root = null;

	/**
	 * Returns the number of leaves in this tree.
	 * 
	 * @return value of {@link #numLeaves}.
	 */
	public int getLeafCount() {
		return getRoot().getNumberLeaves();
	}
	
	
	public String toNewick() {
		return root.toNewick() + ";";
	}
	
	public String toNHX() {
		return nodeToNHX(root) + ";";
	}
	
	public String toNHXFile() {
	    return "#NEXUS\nbegin trees\ntree TREE1=[&R]" + toNHX() + "\nend;\n";
	}
	
	public String nodeToNHX(TreeNode node) {
		if (node.isLeaf()) {
			List<String> c = new ArrayList<String>();
			for (String key: leafProperties.keySet()) {
				c.add(key + "=\"" + leafProperties.get(key)[node.getLeafKey()] + "\"");
			}
			return TreeNode.escapeName(node.getName()) + "[&" + StringUtils.join(c, ",") + "]";
		} else {
			List<String> c = new ArrayList<String>();
			for (TreeNode child: node.getChildren()) {
				c.add(nodeToNHX(child));
			}
			return "(" + StringUtils.join(c, ",") + ")" + TreeNode.escapeName(node.getName());
		}		
	}
	
	public void addLeafProperties(String name, String[] properties) {
		this.leafProperties.put(name, properties);
	}
	
	public void mapLeafKeys(List<String> list) {
		int i = 0; 
        for (TreeNode leaf: getRoot().getLeaves()) {
        	leaf.setLeafKey(list.indexOf(leaf.getName()));
        	if (leaf.getLeafKey() != -1) i++;
        }		
        System.out.println("leaf key set: " + i);
	}
	
	public void trimTree() {
		// trim tree, remove nodes without leaf key
		boolean leafRemoved = false;
		do {
			leafRemoved = false;
			for (TreeNode node: getRoot().getLeaves()) {
				if (node.getLeafKey() == -1) {
					node.getParent().getChildren().remove(node);
					leafRemoved = true;
				}
			}
			if (leafRemoved) getRoot().resetLeaves();
			System.out.println(getRoot().getLeaves().size());
		} while (leafRemoved && getRoot().getLeaves().size() > 1);
		if (getRoot().getLeaves().size() == 1) {
			log.log(Level.WARNING, "Tree has no leaf nodes with a valid leaf key (mapped to a column). ");
		}		
	}
}
