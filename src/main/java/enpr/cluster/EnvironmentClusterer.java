package enpr.cluster;

import cern.colt.matrix.linalg.Algebra;
import ch.usi.inf.sape.hac.ClusteringMatrixBuilder;
import ch.usi.inf.sape.hac.HierarchicalAgglomerativeClusterer;
import ch.usi.inf.sape.hac.agglomeration.AgglomerationMethod;
import ch.usi.inf.sape.hac.agglomeration.WardLinkage;
import ch.usi.inf.sape.hac.dendrogram.Dendrogram;
import ch.usi.inf.sape.hac.dendrogram.DendrogramBuilder;
import ch.usi.inf.sape.hac.experiment.DissimilarityMeasure;
import ch.usi.inf.sape.hac.experiment.Experiment;
import enpr.Profile;
import enpr.similarity.JaccardIndex;
import enpr.similarity.SimilarityMeasure;
import enpr.tree.DendrogramTree;
import enpr.tree.Tree;
import enpr.tree.TreeNode;

public class EnvironmentClusterer implements Experiment, DissimilarityMeasure {
	private Profile profile;
	private SimilarityMeasure similarityMeasure = new JaccardIndex();
	private AgglomerationMethod agglomerationMethod = new WardLinkage();
	
	public EnvironmentClusterer() {
		super();
		this.similarityMeasure = new JaccardIndex();
	}
	
	public EnvironmentClusterer(Profile profile) {
		this.profile = profile;
		this.similarityMeasure = new JaccardIndex();
		this.similarityMeasure.setMatrix(new Algebra().transpose(profile.getProfile()).toArray());
	}
	
	public void setProfile(Profile profile) {
		this.profile = profile;
		this.similarityMeasure.setMatrix(new Algebra().transpose(profile.getProfile()).toArray());		
	}

	@Override
	public double computeDissimilarity(Experiment experiment, int obs1, int obs2) {
		return 1.0-similarityMeasure.similarity(obs1, obs2);
	}

	@Override
	public int getNumberOfObservations() {
		return profile.getColNames().length;
	}
	
	public Dendrogram clusterDendrogram() {
		DendrogramBuilder dendrogramBuilder = new DendrogramBuilder(this.getNumberOfObservations());
		HierarchicalAgglomerativeClusterer clusterer = new HierarchicalAgglomerativeClusterer(this, this, agglomerationMethod);
		clusterer.cluster(dendrogramBuilder);
		Dendrogram dendrogram = dendrogramBuilder.getDendrogram();
		return dendrogram;
	}
	
	public Tree clusterTree() {
		Dendrogram d = clusterDendrogram();
		Tree tree = new DendrogramTree(d);
		for (TreeNode leaf: tree.getRoot().getLeaves()) {
			leaf.setName(profile.getColNames()[leaf.getLeafKey()]);
		}
		return tree;
	}
	
	public int[][] clusterMatrix() {
		ClusteringMatrixBuilder matrixBuilder = new ClusteringMatrixBuilder(this.getNumberOfObservations());
		HierarchicalAgglomerativeClusterer clusterer = new HierarchicalAgglomerativeClusterer(this, this, agglomerationMethod);
		clusterer.cluster(matrixBuilder);
		int[][] clustering = matrixBuilder.getClustering();
		return clustering;		
	}
	
	/**
	 * @return the agglomerationMethod
	 */
	public AgglomerationMethod getAgglomerationMethod() {
		return agglomerationMethod;
	}

	/**
	 * @param agglomerationMethod the agglomerationMethod to set
	 */
	public void setAgglomerationMethod(AgglomerationMethod agglomerationMethod) {
		this.agglomerationMethod = agglomerationMethod;
	}

	/**
	 * @return the similarityMeasure
	 */
	public SimilarityMeasure getSimilarityMeasure() {
		return similarityMeasure;
	}

	/**
	 * @param similarityMeasure the similarityMeasure to set
	 */
	public void setSimilarityMeasure(SimilarityMeasure similarityMeasure) {
		this.similarityMeasure = similarityMeasure;
		this.similarityMeasure.setMatrix(new Algebra().transpose(profile.getProfile()).toArray());		
	}
}
