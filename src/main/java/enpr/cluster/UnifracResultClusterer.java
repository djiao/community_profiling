package enpr.cluster;

import ch.usi.inf.sape.hac.ClusteringMatrixBuilder;
import ch.usi.inf.sape.hac.HierarchicalAgglomerativeClusterer;
import ch.usi.inf.sape.hac.agglomeration.AgglomerationMethod;
import ch.usi.inf.sape.hac.agglomeration.WardLinkage;
import ch.usi.inf.sape.hac.dendrogram.Dendrogram;
import ch.usi.inf.sape.hac.dendrogram.DendrogramBuilder;
import ch.usi.inf.sape.hac.experiment.DissimilarityMeasure;
import ch.usi.inf.sape.hac.experiment.Experiment;
import enpr.tree.DendrogramTree;
import enpr.tree.Tree;
import enpr.tree.TreeNode;

public class UnifracResultClusterer implements Experiment, DissimilarityMeasure {
	private double[][] distance;
	private String[] ids;
	private AgglomerationMethod agglomerationMethod = new WardLinkage();
	
	public UnifracResultClusterer(double[][] distance, String[] ids) {
		this.distance = distance;
		this.ids = ids;
	}
	
	@Override
	public double computeDissimilarity(Experiment experiment, int obs1, int obs2) {
		return distance[obs1][obs2];
	}

	@Override
	public int getNumberOfObservations() {
		return ids.length;
	}	

	public Dendrogram clusterDendrogram() {
		DendrogramBuilder dendrogramBuilder = new DendrogramBuilder(this.getNumberOfObservations());
		HierarchicalAgglomerativeClusterer clusterer = new HierarchicalAgglomerativeClusterer(this, this, agglomerationMethod);
		clusterer.cluster(dendrogramBuilder);
		Dendrogram dendrogram = dendrogramBuilder.getDendrogram();
		return dendrogram;
	}
	
	public Tree clusterTree() {
		Dendrogram d = clusterDendrogram();
		Tree tree = new DendrogramTree(d);
		for (TreeNode leaf: tree.getRoot().getLeaves()) {
			leaf.setName(ids[leaf.getLeafKey()]);
		}
		return tree;
	}
	
	public int[][] clusterMatrix() {
		ClusteringMatrixBuilder matrixBuilder = new ClusteringMatrixBuilder(this.getNumberOfObservations());
		HierarchicalAgglomerativeClusterer clusterer = new HierarchicalAgglomerativeClusterer(this, this, agglomerationMethod);
		clusterer.cluster(matrixBuilder);
		int[][] clustering = matrixBuilder.getClustering();
		return clustering;		
	}
	
	/**
	 * @return the agglomerationMethod
	 */
	public AgglomerationMethod getAgglomerationMethod() {
		return agglomerationMethod;
	}

	/**
	 * @param agglomerationMethod the agglomerationMethod to set
	 */
	public void setAgglomerationMethod(AgglomerationMethod agglomerationMethod) {
		this.agglomerationMethod = agglomerationMethod;
	}
}
