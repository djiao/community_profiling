package enpr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PatternOptionBuilder;
import org.apache.commons.cli.PosixParser;

import cern.colt.function.DoubleFunction;
import enpr.cluster.EnvironmentClusterer;
import enpr.io.MatrixVectorReader;
import enpr.transform.BinaryReverse;
import enpr.tree.Tree;

/**
 * A command line based Tree builder. 
 * 
 * @author djiao
 */
public class TreeBuilder {
	private static final Logger logger = Logger.getLogger(TreeBuilder.class.getName());

	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		CommandLineParser parser = new PosixParser();
		File profFile = null;
		File outFile = null;
		String type = null;
		DoubleFunction transformer = null;
		double cutoff = 1e-5;
		Class<DoubleFunction> cls = null;
		
		Options options = new Options();
		options.addOption(App.buildOption("h", "help",        false, "print help", false, null));
		options.addOption(App.buildOption("p", "profile",     true, "profile matrix (required)", true, PatternOptionBuilder.EXISTING_FILE_VALUE));
		options.addOption(App.buildOption("r", "transformer", true, "value transformer (optional, default enpr.transform.BinaryReverse)", false, PatternOptionBuilder.CLASS_VALUE));
		options.addOption(App.buildOption("c", "cutoff",      true, "cutoff value if transformer is enpr.transform.BinaryReverse. (optional, default 1e-5)", false, PatternOptionBuilder.NUMBER_VALUE));
		options.addOption(App.buildOption("o", "output",      true, "output tree file (required)", true, PatternOptionBuilder.FILE_VALUE));
		options.addOption(App.buildOption("t", "tree-type",   true, "tree type [newick | nhx] (optional, default newick)", false, PatternOptionBuilder.STRING_VALUE));
		
		try {
			CommandLine line = parser.parse(options, args);
			
			if (line.hasOption('h')) {
				App.printHelp(options, TreeBuilder.class.getName());
				System.exit(0);
			}
			profFile = (File) line.getParsedOptionValue("p");
			outFile = (File)line.getParsedOptionValue("o");
			type = (String)line.getOptionValue("t", "newick");
			
			if (line.hasOption('r')) {
				if (line.hasOption('c')) {
					cutoff = ((Number)line.getParsedOptionValue("c")).doubleValue();
				}
				cls = (Class<DoubleFunction>)line.getParsedOptionValue("r");
				if (cls.getName().equals(BinaryReverse.class.getName())) {
					transformer = new BinaryReverse(cutoff);
				} else {
					transformer = (DoubleFunction)cls.newInstance();
				}
			} else {
				transformer = new BinaryReverse(cutoff);
			}
		} catch (ParseException ex) {
			logger.log(Level.SEVERE, "Unexpected exception: {0}", ex.getMessage());
			App.printHelp(options, TreeBuilder.class.getName());
			System.exit(1);
		} catch (InstantiationException e) {
			logger.log(Level.SEVERE, "Can't start transformer class {0}: {1}", new Object[]{cls.getName(), e.getMessage()});
			System.exit(1);
		} catch (IllegalAccessException e) {
			logger.log(Level.SEVERE, "Can't start transformer class {0}: {1}", new Object[]{cls.getName(), e.getMessage()});
			System.exit(1);
		} 
		
		MatrixVectorReader reader = null;
		Profile profile = new Profile();
		List<String> colNames = new ArrayList<String>(); 
		List<String> rowNames = new ArrayList<String>();		
		try {
			if (profFile.getName().endsWith("gz")) {
				reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(profFile))));
			} else if (profFile.getName().endsWith("zip")) {
				reader = new MatrixVectorReader(new InputStreamReader(new ZipInputStream(new FileInputStream(profFile))));
			} else {
				reader = new MatrixVectorReader(new FileReader(profFile));
			}
			double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
			profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
			profile.setColNames(colNames.toArray(new String[colNames.size()]));
			profile.setProfile(matrix);							
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Error in reading file {0}: {1}", new Object[] {profFile.getAbsolutePath(), ex.getMessage()});
			System.exit(1);
		}
		
		profile.setProfile(profile.getProfile().assign(transformer));
		EnvironmentClusterer clusterer = new EnvironmentClusterer(profile);
		Tree tree = clusterer.clusterTree();
		try {
			PrintWriter pw = new PrintWriter(new FileWriter(outFile));
			if ("newick".equals(type)) {
				pw.write(tree.toNewick());
			} else if ("nhx".equals(type)){
				pw.write(tree.toNHX());
			}
			pw.close();
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Error in writing file {0}: {1}", new Object[] { outFile.getAbsolutePath(), ex.getMessage()});
			System.exit(1);
		}
	}
}
