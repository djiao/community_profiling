package enpr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;

import com.google.common.primitives.Ints;

import enpr.tree.Tree;

public class Profile {
	/**
	 * A hierarchical clustering tree of the samples 
	 */
	private Tree tree;
	/**
	 * Row names in the profile
	 */
	private String[] rowNames;
	/**
	 * Column names in the profile.  
	 */
	private String[] colNames;
	/**
	 * 2 dimensional profile matrix. Rows are proteins and columns are samples. 
	 */
	private DoubleMatrix2D profile;
	
	/**
	 * @return the tree
	 */
	public Tree getTree() {
		return tree;
	}
	/**
	 * @param tree the tree to set
	 */
	public void setTree(Tree tree) {
		this.tree = tree;
	}
	/**
	 * @return the sampleList
	 */
	public String[] getRowNames() {
		return rowNames;
	}
	/**
	 * @param sampleList the sampleList to set
	 */
	public void setRowNames(String[] rows) {
		this.rowNames = rows;
	}
	/**
	 * @return the proteinList
	 */
	public String[] getColNames() {
		return colNames;
	}
	/**
	 * @param proteinList the proteinList to set
	 */
	public void setColNames(String[] colNames) {
		this.colNames = colNames;
	}
	/**
	 * @return the profile
	 */
	public DoubleMatrix2D getProfile() {
		return profile;
	}
	/**
	 * @param profile the profile to set
	 */
	public void setProfile(double[][] profile) {
		this.profile = new DenseDoubleMatrix2D(profile);
	}
	/**
	 * @param profile the profile to set
	 */
	public void setProfile(DoubleMatrix2D profile) {
		this.profile = profile;
	} 
	
	public void transpose() {
		this.profile = new Algebra().transpose(this.profile);
		String[] tmp = this.colNames;
		this.colNames = this.rowNames;
		this.rowNames = tmp;
	}
	
	public void selectRows(int[] rows) {
		List<String> newRowList = new ArrayList<String>();
		for (int i : rows) {
			newRowList.add(rowNames[i]);
		}
		this.rowNames = newRowList.toArray(new String[newRowList.size()]);
		int[] colInd = new int[colNames.length];
		for (int i = 0; i < colNames.length; i++) {
			colInd[i] = i;
		}
		this.profile = profile.viewSelection(rows, colInd);
	}
	
	public void selectColumns(int[] cols) {
		List<String> newColList = new ArrayList<String>();
		for (int i : cols) {
			newColList.add(colNames[i]);
		}
		this.rowNames = newColList.toArray(new String[newColList.size()]);
		int[] rowInd = new int[rowNames.length];
		for (int i = 0; i < rowNames.length; i++) {
			rowInd[i] = i;
		}
		this.profile = profile.viewSelection(rowInd, cols);		
	}
	
	public void selectRows(String[] rows) {
		List<Integer> index = new ArrayList<Integer>();
		List<String> rowNameList = new ArrayList<String>();
		for (String row : rowNames) {
			rowNameList.add(row);
		}
		List<String> newList = new ArrayList<String>();
		for (int i = 0; i < rows.length; i++) {
			int id = rowNameList.indexOf(rows[i]);
			if (id >= 0) {
				index.add(id);
				newList.add(rows[i]);
			}
		}
		int[] rowInd = Ints.toArray(index);
		int[] colInd = new int[colNames.length];
		for (int i = 0; i < colNames.length; i++) {
			colInd[i] = i;
		}
		this.profile = profile.viewSelection(rowInd, colInd);
		this.rowNames = newList.toArray(new String[newList.size()]);
	}
	
	public void selectColumns(String[] cols) {
		List<Integer> index = new ArrayList<Integer>();
		List<String> colNameList = Arrays.asList(colNames);
		List<String> newList = new ArrayList<String>();
		for (int i = 0; i < cols.length; i++) {
			int id = colNameList.indexOf(cols[i]);
			if (id >= 0) {
				index.add(id);
				newList.add(cols[i]);
			}
		}
		int[] colInd = Ints.toArray(index);
		int[] rowInd = new int[rowNames.length];
		for (int i = 0; i < rowNames.length; i++) {
			rowInd[i] = i;
		}
		this.profile = profile.viewSelection(rowInd, colInd);
		this.colNames = newList.toArray(new String[newList.size()]);		
	}
}
