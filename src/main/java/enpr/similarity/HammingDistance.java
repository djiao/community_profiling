package enpr.similarity;

public class HammingDistance extends AbstractSimilarityMeasure {

	@Override
	public double similarity(double[] p1, double[] p2) {
		double m = 0.0;
		double d = 0.0;
		for (int i = 0; i < p1.length; i++) {
			if (p1[i] == 1.0 && p2[i] == 1.0) m += 1.0;
			else if (p1[i] != p2[i]) d += 1.0; 
		}
		return (m/p1.length >= 0.1) ? p1.length - d : 0;
	}
}
