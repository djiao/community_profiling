package enpr.similarity;

public class SpearmansCorrelation extends AbstractSimilarityMeasure {
	private org.apache.commons.math.stat.correlation.SpearmansCorrelation calculator = 
			new org.apache.commons.math.stat.correlation.SpearmansCorrelation();
	@Override
	public double similarity(double[] p1, double[] p2) {
		return calculator.correlation(p1, p2);
	}

}
