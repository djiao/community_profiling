package enpr.similarity;

import cern.colt.matrix.DoubleMatrix1D;

public abstract class AbstractSimilarityMeasure implements SimilarityMeasure {
    protected double[][] matrix;

    @Override
    public void setMatrix(double[][] matrix) {
        this.matrix = matrix;
    }

    @Override
    public double similarity(int r1, int r2) {
        return similarity(matrix[r1], matrix[r2]);
    }

    @Override
    public double apply(DoubleMatrix1D x, DoubleMatrix1D y) {
        return 1.0 - similarity(x.toArray(), y.toArray());
    }
}
