package enpr.similarity;

import org.apache.mahout.common.Pair;

import cern.colt.matrix.DoubleMatrix1D;


/**
 * Mutual information based distance measure
 * 
 * @author djiao
 */
public class MutualInformation implements SimilarityMeasure {
	private int[][] binIndexes;
	private double[][] marginalProbs;	
	
	private int nbin = 10;
	
	public MutualInformation() {
		super();
	}
	
	public MutualInformation(int nbin) {
		super();
		this.nbin = nbin;
	}
	
	public Pair<int[], int[]> binning(double[] d) {
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;	
		for (int i = 0; i < d.length; i++) {
			if (max < d[i]) max = d[i];
			if (min > d[i]) min = d[i];
		}
		int[] bin = new int[nbin];
		int[] index = new int[d.length];
		double bw = (max - min)/nbin;
		for (int i = 0; i < d.length; i++) {
			int bin_index = 0;
			if (bw > 0.0) {
				bin_index = (int)((d[i]-min)/bw);
				if (bin_index == nbin) bin_index--;
			}
			bin[bin_index]++;
			index[i] = bin_index;
		}
		return new Pair<int[], int[]>(bin, index);
	}
	
	@Override
	public double similarity(double[] d1, double[] d2) {
		Pair<int[], int[]> l1 = binning(d1);
		Pair<int[], int[]> l2 = binning(d2);
		int[] bin1 = l1.getFirst();
		int[] bin2 = l2.getFirst();
		int[] index1 = l1.getSecond();
		int[] index2 = l2.getSecond();
		
		double[] p1 = new double[nbin];
		double[] p2 = new double[nbin];
		for (int i = 0; i < nbin; i++) {
			p1[i] = bin1[i]/(double)d1.length;
			p2[i] = bin2[i]/(double)d2.length;
		}
		
		int[][] bin12 = new int[nbin][nbin];
		for (int i = 0; i < d1.length; i++) {
			bin12[index1[i]][index2[i]]++;
		}
		
		double mi = 0.0d;
		for (int i = 0; i < nbin; i++) {
			for (int j = 0; j < nbin; j++) {
				double p_ij = bin12[i][j]/(double)d1.length;
				if (p_ij != 0 && p1[i] != 0 && p2[j] != 0) {
					mi += p_ij * Math.log(p_ij/(p1[i]*p2[j]));
				}
			}
		}
		return mi;
	}
	

	@Override
	public void setMatrix(double[][] matrix) {
		this.binIndexes = new int[matrix.length][];
		this.marginalProbs = new double[matrix.length][];
		
		int n = matrix[0].length;
		for (int i = 0; i < matrix.length; i++) {
			Pair<int[], int[]> pair = binning(matrix[i]);
			binIndexes[i] = pair.getSecond();
			int[] bin = pair.getFirst();
			double[] pr = new double[nbin];
			for (int j = 0; j < nbin; j++) {
				pr[j] = bin[j]/(double)n;
			}
			marginalProbs[i] = pr;
		}
	}

	@Override
	public double similarity(int r1, int r2) {
		double[] p1 = marginalProbs[r1];
		double[] p2 = marginalProbs[r2];
		int[] index1 = binIndexes[r1];
		int[] index2 = binIndexes[r2];
		
		// calculate joint probability
		int[][] bin12 = new int[nbin][nbin];
		for (int i = 0; i < index1.length; i++) {
			bin12[index1[i]][index2[i]]++;
		}
		
		double mi = 0.0d;
		for (int i = 0; i < nbin; i++) {
			for (int j = 0; j < nbin; j++) {
				double p_ij = bin12[i][j]/(double)index1.length;
				if (p_ij != 0 && p1[i] != 0 && p2[j] != 0) {
					mi += p_ij * Math.log(p_ij/(p1[i]*p2[j]));
				}
			}
		}
		return mi;
	}

	@Override
	public double apply(DoubleMatrix1D x, DoubleMatrix1D y) {
		// Reference: H. Joe, Relative Entropy Measures of Multivariate Dependence, JASA, 1989, 157-164.
		return 1.0 - Math.sqrt(1.0 - Math.exp(-2*similarity(x.toArray(), y.toArray())));
	}
}
