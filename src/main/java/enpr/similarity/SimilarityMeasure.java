package enpr.similarity;

import cern.colt.matrix.doublealgo.Statistic.VectorVectorFunction;
import weka.core.DistanceFunction;

/**
 * Measures distance
 * 
 * @author djiao
 */
public interface SimilarityMeasure extends VectorVectorFunction {
	public abstract void setMatrix(double[][] matrix); 
	
	public abstract double similarity(double[] p1, double[] p2);
	
	public abstract double similarity(int r1, int r2);
}
