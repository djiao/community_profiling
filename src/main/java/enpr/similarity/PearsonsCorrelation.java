package enpr.similarity;

public class PearsonsCorrelation extends AbstractSimilarityMeasure {
	private org.apache.commons.math.stat.correlation.PearsonsCorrelation calculator = 
			new org.apache.commons.math.stat.correlation.PearsonsCorrelation();
	@Override
	public double similarity(double[] p1, double[] p2) {
		return calculator.correlation(p1, p2);
	}
}
