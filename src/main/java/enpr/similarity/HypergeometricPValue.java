package enpr.similarity;

import org.apache.commons.math3.distribution.HypergeometricDistribution;

public class HypergeometricPValue extends AbstractSimilarityMeasure {

	@Override
	public double similarity(double[] p1, double[] p2) {
		int x=0, y=0, z = 0;
		for (int i = 0; i < p1.length; i++) {
			if (p1[i] == 1.0) x++;
			if (p2[i] == 1.0) y++;
			if (p1[i] == 1.0 && p2[i] == 1.0)  z++;
		}
		
		if (x == 0 || y == 0 || z == 0) {
			return 0.0;
		}
		
		// Calculate hypergenometric p-value
		double p = 0.0;
		int N = p1.length;
		HypergeometricDistribution hg = new HypergeometricDistribution(N, x, y);
		/*
		for (int i = z+1; i <= Math.min(x, y); i++) {
			p += Arithmetic.binomial(x, i) * Arithmetic.binomial(N-x, y-i)
					/ Arithmetic.binomial(N, y);
		}
		*/
		for (int i = z+1; i < Math.min(x, y); i++) {
			p += hg.probability(i);
		}
		return 1-p;
	}
}
