package enpr.similarity;


public class JaccardIndex extends AbstractSimilarityMeasure {
	
	@Override
	public double similarity(double[] p1, double[] p2) {
        return jaccardIndex(p1, p2);		
	}
	
	public double jaccardIndex(double[] p1, double[] p2) {
		double n1 = 0, n2 = 0, n12 = 0;
		for (int i = 0; i < p1.length; i++) {
			n1 += p1[i];
			n2 += p2[i];
			n12 += p1[i]*p2[i];
		}
		if (n12 == 0) return 0.0;
		return n12/(n1+n2-n12);
	}
}
