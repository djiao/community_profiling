package enpr.dist;

import cern.colt.function.IntIntDoubleFunction;
import cern.colt.list.DoubleArrayList;
import cern.colt.list.IntArrayList;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.doublealgo.Statistic;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;
import enpr.Profile;
import enpr.io.MatrixVectorReader;
import enpr.similarity.MutualInformation;
import enpr.transform.NegativeLog;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.math3.util.Pair;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.zip.GZIPInputStream;

/**
 * Breaks the original matrix into chunks and calculate the distance matrix between pair of chunks then merge
 */
public class ChunkThreadedDistance extends Distance {
    private int chunkSize = 10000;
    private int nThreads = 10;
    private DoubleMatrix2D matrix;
    private double threshold;
    private boolean biggerThan;
    private Statistic.VectorVectorFunction function;

    public ChunkThreadedDistance(int chunkSize, int nThreads) {
        this.chunkSize = chunkSize;
        this.nThreads = nThreads;
    }

    @Override
    public SparseDoubleMatrix2D distance(DoubleMatrix2D matrix, Statistic.VectorVectorFunction function, double threshold, boolean biggerThan) {
        this.matrix = matrix;
        this.threshold = threshold;
        this.function = function;
        this.biggerThan = biggerThan;
        SparseDoubleMatrix2D result = new SparseDoubleMatrix2D(matrix.rows(), matrix.rows());

        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        int nChunks = (int)Math.ceil(matrix.rows()/(double)chunkSize);
        Future[][] futures = new Future[nChunks][nChunks];
        for (int i = 0; i < nChunks; i++) {
            for (int j = i; j < nChunks; j++) {
                Callable<DoubleMatrix2D> worker = new ChunkDistanceCallable(i, j, false);
                Future<DoubleMatrix2D> submit = executor.submit(worker);
                futures[i][j] = submit;
            }
        }
        for (int i1 = 0; i1 < nChunks; i1++) {
            for (int i2 = i1; i2 < nChunks; i2++) {
                Future<DoubleMatrix2D> f = futures[i1][i2];
                final int f1 = i1 * chunkSize;
                final int f2 = i2 * chunkSize;
                IntArrayList rows = new IntArrayList();
                IntArrayList cols = new IntArrayList();
                DoubleArrayList vals = new DoubleArrayList();
                try {
                    DoubleMatrix2D d = f.get();
                    d.getNonZeros(rows, cols, vals);
                    synchronized (result) {
                        for (int i = 0; i < rows.size(); i++) {
                            result.setQuick(f1+rows.get(i), f2+cols.get(i), vals.get(i));
                            result.setQuick(f2+cols.get(i), f1+rows.get(i), vals.get(i));
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    @Override
    public DenseDoubleMatrix2D distance(DoubleMatrix2D matrix, Statistic.VectorVectorFunction function) {
        this.matrix = matrix;
        this.function = function;
        DenseDoubleMatrix2D result = new DenseDoubleMatrix2D(matrix.rows(), matrix.rows());

        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        int nChunks = (int)Math.ceil(matrix.rows()/(double)chunkSize);
        List<Pair<Pair<Integer, Integer>, Future<DoubleMatrix2D>>> list = new ArrayList();
        for (int i = 0; i < nChunks; i++) {
            for (int j = i; j < nChunks; j++) {
                Callable<DoubleMatrix2D> worker = new ChunkDistanceCallable(i, j, true);
                Future<DoubleMatrix2D> submit = executor.submit(worker);
                list.add(new Pair<>(new Pair<>(i, j), submit));
            }
        }
        for (Pair<Pair<Integer, Integer>, Future<DoubleMatrix2D>> f : list) {
            synchronized (result) {
                try {
                    int i1 = f.getKey().getKey();
                    int i2 = f.getKey().getValue();

                    int f1 = i1*chunkSize;
                    int t1 = (i1+1) * chunkSize;
                    if (t1 > matrix.rows()) t1 = matrix.rows();

                    int f2 = i2*chunkSize;
                    int t2 = (i2+1) * chunkSize;
                    if (t2 > matrix.rows()) t2 = matrix.rows();

                    new Algebra().subMatrix(result, f1, t1-1, f2, t2-1).assign(f.getValue().get());

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    @Override
    public void write(DoubleMatrix2D matrix, Statistic.VectorVectorFunction function, double threshold, boolean biggerThan, final Writer out) {
        this.matrix = matrix;
        this.threshold = threshold;
        this.function = function;

        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        int nChunks = (int)Math.ceil(matrix.rows()/(double)chunkSize);
        for (int i = 0; i < nChunks; i++) {
            for (int j = i; j < nChunks; j++) {
                Runnable worker = new ChunkDistanceRunnable(i, j, out);
                executor.execute(worker);
            }
        }
        try {
            executor.shutdown();
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException iex) {
            iex.printStackTrace();
        }
    }

    @Override
    public void write(DoubleMatrix2D matrix, final List<String> names, Statistic.VectorVectorFunction function, double threshold, boolean biggerThan, final Writer out) {
        this.matrix = matrix;
        this.threshold = threshold;
        this.function = function;

        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        int nChunks = (int)Math.ceil(matrix.rows()/(double)chunkSize);
        for (int i = 0; i < nChunks; i++) {
            for (int j = i; j < nChunks; j++) {
                Runnable worker = new ChunkDistanceNameRunnable(i, j, out, names);
                executor.execute(worker);
            }
        }
        try {
            executor.shutdown();
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException iex) {
            iex.printStackTrace();
        }
    }

    @Override
    public void write(DoubleMatrix2D matrix, Statistic.VectorVectorFunction function, Writer out) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public class ChunkDistanceRunnable extends ChunkDistanceCallable implements Runnable  {
        private Writer out;

        public ChunkDistanceRunnable(int i1, int i2, Writer out) {
            super(i1, i2, false);
            this.out = out;
        }

        @Override
        public void run() {
            final int f1 = i1*chunkSize;
            final int f2 = i2*chunkSize;
            DoubleMatrix2D val = call();
            synchronized (out) {
                val.forEachNonZero(new IntIntDoubleFunction() {
                    @Override
                    public double apply(int i1, int i2, double v) {
                        try {
                            out.write((f1+i1) + "\t" + (f2+i2) + "\t" + String.format("%6.3e", v) + "\n");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        return 0.0;
                    }
                });
                try {
                    this.out.flush();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public class ChunkDistanceNameRunnable extends ChunkDistanceCallable implements Runnable  {
        private Writer out;
        private List<String> names;

        public ChunkDistanceNameRunnable(int i1, int i2, Writer out, List<String> names) {
            super(i1, i2, false);
            this.out = out;
            this.names = names;
        }

        @Override
        public void run() {
            final int f1 = i1*chunkSize;
            final int f2 = i2*chunkSize;
            DoubleMatrix2D val = call();
            synchronized (out) {
                val.forEachNonZero(new IntIntDoubleFunction() {
                    @Override
                    public double apply(int i1, int i2, double v) {
                        try {
                            out.write(names.get(f1+i1) + "\t" + names.get(f2+i2) + "\t" + String.format("%6.3e", v) + "\n");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        return 0.0;
                    }
                });
                try {
                    this.out.flush();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public class ChunkDistanceCallable implements Callable<DoubleMatrix2D> {

        protected int i1, i2;
        protected boolean dense = false;

        public ChunkDistanceCallable(int i1, int i2, boolean dense) {
            this.i1 = i1;
            this.i2 = i2;
            this.dense = dense;
        }


        @Override
        public DoubleMatrix2D call() {
            int f1 = i1*chunkSize;
            int t1 = Math.min((i1+1) * chunkSize, matrix.rows());
            DoubleMatrix2D chunk1 = new Algebra().subMatrix(matrix, f1, t1-1, 0, matrix.columns()-1);
            DoubleMatrix2D result;
            if (i1 == i2) {
                result = dense? new DenseDoubleMatrix2D(t1-f1, t1-f1): new SparseDoubleMatrix2D(t1-f1, t1-f1);
                for (int i = 0; i < chunk1.rows(); i++) {
                    for (int j = i+1; j < chunk1.rows(); j++) {
                        double d = function.apply(chunk1.viewRow(i), chunk1.viewRow(j));
                        if (dense) {
                            result.setQuick(i, j, d);
                            result.setQuick(j, i, d);
                        } else {
                            if ((biggerThan && d > threshold) || (!biggerThan && d < threshold)) {
                                result.setQuick(i, j, d);
                            }
                        }
                    }
                }
            } else {
                int f2 = i2*chunkSize;
                int t2 = (i2+1) * chunkSize;
                if (t2 > matrix.rows()) t2 = matrix.rows();
                result = dense? new DenseDoubleMatrix2D(t1-f1, t2-f2): new SparseDoubleMatrix2D(t1-f1, t2-f2);
                DoubleMatrix2D chunk2 = new Algebra().subMatrix(matrix, f2, t2-1, 0, matrix.columns()-1);
                for (int i = 0; i < chunk1.rows(); i++) {
                    for (int j = 0; j < chunk2.rows(); j++) {
                        double d = function.apply(chunk1.viewRow(i), chunk2.viewRow(j));
                        if (dense) {
                            result.setQuick(i, j, d);
                        } else if ((biggerThan && d > threshold) || (!biggerThan && d < threshold)) {
                            result.setQuick(i, j, d);
                        }
                    }
                }
            }
            return result;
        }
    }

    // usage: java ChunkThreadedDistance <prof> <out> <chunk_size> <n_threads> <threshold>
    public static void main(String[] args) throws Exception {
        List<String> colNames = new ArrayList<>();
        List<String> rowNames = new ArrayList<>();

        MatrixVectorReader reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(args[0]))));

        double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);

        Profile profile = new Profile();
        profile.setProfile(matrix);

        System.out.println("Size:" + rowNames.size() + ", " + colNames.size());
        NegativeLog transformer  = new NegativeLog();
        DoubleMatrix2D mat = new DenseDoubleMatrix2D(matrix).assign(transformer);
        profile.setProfile(profile.getProfile().assign(transformer));

        ChunkThreadedDistance threads = new ChunkThreadedDistance(Integer.parseInt(args[2]), Integer.parseInt(args[3]));
        System.out.println("Start pairwise similarity calculation...");
        FileWriter out = new FileWriter(args[1]);
        threads.write(mat, new MutualInformation(), Double.parseDouble(args[4]), false, out);
    }
}
