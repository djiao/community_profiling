package enpr.dist;

import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.doublealgo.Statistic;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;

import java.io.Writer;
import java.util.List;

/**
 * Calculate distance matrices
 */
public abstract class Distance {
    public abstract SparseDoubleMatrix2D distance(DoubleMatrix2D matrix, Statistic.VectorVectorFunction function, double threshold, boolean biggerThan);
    public abstract DenseDoubleMatrix2D distance(DoubleMatrix2D matrix, Statistic.VectorVectorFunction function);
    public abstract void write(DoubleMatrix2D matrix, Statistic.VectorVectorFunction function, double threshold, boolean biggerThan, Writer out);
    public abstract void write(DoubleMatrix2D matrix, List<String> names, Statistic.VectorVectorFunction function, double threshold, boolean biggerThan, final Writer out);
    public abstract void write(DoubleMatrix2D matrix, Statistic.VectorVectorFunction function, Writer out);
}
