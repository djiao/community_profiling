package enpr.dist;

import cern.colt.function.IntIntDoubleFunction;
import cern.colt.list.DoubleArrayList;
import cern.colt.list.IntArrayList;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.doublealgo.Statistic;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.impl.SparseDoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.math3.util.Pair;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Different from ChunkThreadedDistance, this class calculate the distance between rows from two matrices.
 *
 * Breaks the original matrix1 into chunks and calculate the distance matrix1 between pair of chunks then merge
 */
public class TwoMatrixChunkThreadedDistance {
    private int chunkSize = 10000;
    private int nThreads = 10;
    private DoubleMatrix2D matrix1;
    private DoubleMatrix2D matrix2;
    private double threshold;
    private boolean biggerThan;
    private Statistic.VectorVectorFunction function;

    public TwoMatrixChunkThreadedDistance(int chunkSize, int nThreads) {
        this.chunkSize = chunkSize;
        this.nThreads = nThreads;
    }

    public SparseDoubleMatrix2D distance(DoubleMatrix2D matrix1, DoubleMatrix2D matrix2, Statistic.VectorVectorFunction function, double threshold, boolean biggerThan) {
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;

        this.threshold = threshold;
        this.function = function;
        this.biggerThan = biggerThan;
        SparseDoubleMatrix2D result = new SparseDoubleMatrix2D(matrix1.rows(), matrix2.rows());

        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        int nChunks1 = (int)Math.ceil(matrix1.rows()/(double)chunkSize);
        int nChunks2 = (int)Math.ceil(matrix2.rows()/(double)chunkSize);
        Future[][] futures = new Future[nChunks1][nChunks2];
        for (int i = 0; i < nChunks1; i++) {
            for (int j = 0; j < nChunks2; j++) {
                Callable<DoubleMatrix2D> worker = new ChunkDistanceCallable(i, j, false);
                Future<DoubleMatrix2D> submit = executor.submit(worker);
                futures[i][j] = submit;
            }
        }
        for (int i1 = 0; i1 < nChunks1; i1++) {
            for (int i2 = 0; i2 < nChunks2; i2++) {
                Future<DoubleMatrix2D> f = futures[i1][i2];
                final int f1 = i1 * chunkSize;
                final int f2 = i2 * chunkSize;
                IntArrayList rows = new IntArrayList();
                IntArrayList cols = new IntArrayList();
                DoubleArrayList vals = new DoubleArrayList();
                try {
                    DoubleMatrix2D d = f.get();
                    d.getNonZeros(rows, cols, vals);
                    synchronized (result) {
                        for (int i = 0; i < rows.size(); i++) {
                            result.setQuick(f1+rows.get(i), f2+cols.get(i), vals.get(i));
                            result.setQuick(f2+cols.get(i), f1+rows.get(i), vals.get(i));
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public DenseDoubleMatrix2D distance(DoubleMatrix2D matrix1, DoubleMatrix2D matrix2, Statistic.VectorVectorFunction function) {
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.function = function;
        DenseDoubleMatrix2D result = new DenseDoubleMatrix2D(matrix1.rows(), matrix2.rows());

        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        int nChunks1 = (int)Math.ceil(matrix1.rows()/(double)chunkSize);
        int nChunks2 = (int)Math.ceil(matrix2.rows()/(double)chunkSize);
        List<Pair<Pair<Integer, Integer>, Future<DoubleMatrix2D>>> list = new ArrayList();
        for (int i = 0; i < nChunks1; i++) {
            for (int j = 0; j < nChunks2; j++) {
                Callable<DoubleMatrix2D> worker = new ChunkDistanceCallable(i, j, true);
                Future<DoubleMatrix2D> submit = executor.submit(worker);
                list.add(new Pair<>(new Pair<>(i, j), submit));
            }
        }
        for (Pair<Pair<Integer, Integer>, Future<DoubleMatrix2D>> f : list) {
            synchronized (result) {
                try {
                    int i1 = f.getKey().getKey();
                    int i2 = f.getKey().getValue();

                    int f1 = i1*chunkSize;
                    int t1 = (i1+1) * chunkSize;
                    if (t1 > matrix1.rows()) t1 = matrix1.rows();

                    int f2 = i2*chunkSize;
                    int t2 = (i2+1) * chunkSize;
                    if (t2 > matrix2.rows()) t2 = matrix2.rows();

                    new Algebra().subMatrix(result, f1, t1-1, f2, t2-1).assign(f.getValue().get());

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public void write(DoubleMatrix2D matrix1, DoubleMatrix2D matrix2, List<String> names1, List<String> names2, Statistic.VectorVectorFunction function, double threshold, boolean biggerThan, final Writer out) {
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.threshold = threshold;
        this.function = function;

        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        int nChunks1 = (int)Math.ceil(matrix1.rows()/(double)chunkSize);
        int nChunks2 = (int)Math.ceil(matrix2.rows()/(double)chunkSize);
        for (int i = 0; i < nChunks1; i++) {
            for (int j = 0; j < nChunks2; j++) {
                Runnable worker = new ChunkDistanceRunnable(i, j, out, names1, names2);
                executor.execute(worker);
            }
        }
        try {
            executor.shutdown();
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException iex) {
            iex.printStackTrace();
        }
    }

    public class ChunkDistanceRunnable extends ChunkDistanceCallable implements Runnable  {
        private Writer out;
        private List<String> names1, names2;

        public ChunkDistanceRunnable(int i1, int i2, Writer out, List<String> names1, List<String> names2) {
            super(i1, i2, false);
            this.out = out;
            this.names1 = names1;
            this.names2 = names2;
        }

        @Override
        public void run() {
            final int f1 = i1*chunkSize;
            final int f2 = i2*chunkSize;
            DoubleMatrix2D val = call();
            synchronized (out) {
                val.forEachNonZero(new IntIntDoubleFunction() {
                    @Override
                    public double apply(int i1, int i2, double v) {
                        try {
                            out.write(names1.get(f1+i1) + "\t" + names2.get(f2+i2) + "\t" + String.format("%6.3e", v) + "\n");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        return 0.0;
                    }
                });
                try {
                    this.out.flush();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

    }

    public class ChunkDistanceCallable implements Callable<DoubleMatrix2D> {

        protected int i1, i2;
        protected boolean dense = false;

        public ChunkDistanceCallable(int i1, int i2, boolean dense) {
            this.i1 = i1;
            this.i2 = i2;
            this.dense = dense;
        }


        @Override
        public DoubleMatrix2D call() {
            int f1 = i1*chunkSize;
            int t1 = Math.min((i1 + 1) * chunkSize, matrix1.rows());
            DoubleMatrix2D chunk1 = new Algebra().subMatrix(matrix1, f1, t1-1, 0, matrix1.columns()-1);
            DoubleMatrix2D result;

            int f2 = i2*chunkSize;
            int t2 = (i2+1) * chunkSize;
            if (t2 > matrix2.rows()) t2 = matrix2.rows();
            DoubleMatrix2D chunk2 = new Algebra().subMatrix(matrix2, f2, t2-1, 0, matrix2.columns()-1);

            result = dense? new DenseDoubleMatrix2D(t1-f1, t2-f2): new SparseDoubleMatrix2D(t1-f1, t2-f2);
            for (int i = 0; i < chunk1.rows(); i++) {
                for (int j = 0; j < chunk2.rows(); j++) {
                    double d = function.apply(chunk1.viewRow(i), chunk2.viewRow(j));
                    if (dense) {
                        result.setQuick(i, j, d);
                    } else if ((biggerThan && d > threshold) || (!biggerThan && d < threshold)) {
                        result.setQuick(i, j, d);
                    }
                }
            }

            return result;
        }
    }
}
