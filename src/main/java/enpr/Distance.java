package enpr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PatternOptionBuilder;
import org.apache.commons.cli.PosixParser;

import cern.colt.function.DoubleFunction;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.doublealgo.Statistic;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import enpr.io.MatrixVectorReader;
import enpr.io.MatrixVectorWriter;
import enpr.similarity.MutualInformation;
import enpr.similarity.SimilarityMeasure;
import enpr.transform.BinaryReverse;

/**
 * Calculates pairwise distances between rows based on a profile
 * 
 * @author djiao
 *
 */
public class Distance {
	private static final Logger logger = Logger.getLogger(Distance.class.getName());

	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		CommandLineParser parser = new PosixParser();
		File profFile = null;
		File outFile = null;
		DoubleFunction transformer = null;
		double cutoff = 1e-5;
		Class<DoubleFunction> transformerClass = null;
		Class<SimilarityMeasure> similarityClass = null;
		File listFile = null;
		
		Options options = new Options();
		options.addOption(App.buildOption("h", "help",        false, "print help", false, null));
		options.addOption(App.buildOption("p", "profile",     true, "profile matrix (required)", true, PatternOptionBuilder.EXISTING_FILE_VALUE));
		options.addOption(App.buildOption("r", "transformer", true, "value transformer (optional, default null)", false, PatternOptionBuilder.CLASS_VALUE));
		options.addOption(App.buildOption("c", "cutoff",      true, "cutoff value if transformer is enpr.transform.BinaryReverse. (optional, default 1e-5)", false, PatternOptionBuilder.NUMBER_VALUE));
		options.addOption(App.buildOption("o", "output",      true, "output tree file (required)", true, PatternOptionBuilder.FILE_VALUE));
		options.addOption(App.buildOption("s", "similarity",  true, "similarity measure class (optional, default enpr.similarity.MutualInformation", false, PatternOptionBuilder.CLASS_VALUE));
		options.addOption(App.buildOption("l", "sample-list", true, "list of samples/columns in profile (optional, default every column)", false, PatternOptionBuilder.EXISTING_FILE_VALUE));		
		
		try {
			CommandLine line = parser.parse(options, args);
			
			if (line.hasOption('h')) {
				App.printHelp(options, TreeBuilder.class.getName());
				System.exit(0);
			}
			profFile = (File) line.getParsedOptionValue("p");
			outFile = (File)line.getParsedOptionValue("o");
			
			if (line.hasOption('r')) {
				if (line.hasOption('c')) {
					cutoff = ((Number)line.getParsedOptionValue("c")).doubleValue();
				}
				transformerClass = (Class<DoubleFunction>)line.getParsedOptionValue("r");
				if (transformerClass.getName().equals(BinaryReverse.class.getName())) {
					transformer = new BinaryReverse(cutoff);
				} else {
					transformer = transformerClass.newInstance();
				}
			} 
			
			if (line.hasOption('s')) {
				similarityClass = (Class<SimilarityMeasure>)line.getParsedOptionValue("s");
			} 
			
			if (line.hasOption('l')) {
				listFile = (File)line.getParsedOptionValue("l");
			}
		} catch (ParseException ex) {
			logger.log(Level.SEVERE, "Unexpected exception: {0}", ex.getMessage());
			App.printHelp(options, TreeBuilder.class.getName());
			System.exit(1);
		} catch (InstantiationException e) {
			logger.log(Level.SEVERE, "Can't start transformer class {0}: {1}", new Object[]{transformerClass.getName(), e.getMessage()});
			System.exit(1);
		} catch (IllegalAccessException e) {
			logger.log(Level.SEVERE, "Can't start transformer class {0}: {1}", new Object[]{transformerClass.getName(), e.getMessage()});
			System.exit(1);
		}
		
		MatrixVectorReader reader = null;
		Profile profile = new Profile();
		List<String> colNames = new ArrayList<>();
		List<String> rowNames = new ArrayList<>();
		try {
			if (profFile.getName().endsWith("gz")) {
				reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(profFile))));
			} else if (profFile.getName().endsWith("zip")) {
				reader = new MatrixVectorReader(new InputStreamReader(new ZipInputStream(new FileInputStream(profFile))));
			} else {
				reader = new MatrixVectorReader(new FileReader(profFile));
			}
			double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
			profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
			profile.setColNames(colNames.toArray(new String[colNames.size()]));
			profile.setProfile(matrix);
			
			if (listFile != null) {
				List<String> lines = Files.readLines(listFile, Charsets.UTF_8);
				String[] cols = lines.toArray(new String[lines.size()]);
				profile.selectColumns(cols);
			}
			
			if (transformer != null) {
				profile.setProfile(profile.getProfile().assign(transformer));
			}
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Error in reading file {0}: {1}", new Object[] {profFile.getAbsolutePath(), ex.getMessage()});
			System.exit(1);
		} finally {
			try { if (reader != null) reader.close(); } catch (IOException ex) {}
		}

		SimilarityMeasure sm = similarityClass == null? new MutualInformation() : similarityClass.newInstance();
		DoubleMatrix2D dist = Statistic.distance(profile.getProfile().viewDice(), sm);
		
		MatrixVectorWriter writer;
		if (outFile.getName().endsWith("gz")) {
			writer = new MatrixVectorWriter(new GZIPOutputStream(new FileOutputStream(outFile)));
		} else if (profFile.getName().endsWith("zip")) {
			writer = new MatrixVectorWriter(new ZipOutputStream(new FileOutputStream(outFile)));
		} else {
			writer = new MatrixVectorWriter(new FileWriter(outFile));
		}
		writer.printMatrix(dist);
		writer.close();
	}

}
