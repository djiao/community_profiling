package enpr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.mahout.classifier.evaluation.Auc;
import org.apache.mahout.common.Pair;

import cern.colt.function.DoubleFunction;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;
import enpr.similarity.MutualInformation;
import enpr.similarity.SimilarityMeasure;

public class Predictor {
	/**
	 * Similarity measure method. Default value is MutualInformation
	 */
	public SimilarityMeasure method;

	public void setMethod(SimilarityMeasure method) {
		this.method = method;
	}

	public double[] similarity(double[][] matrix, int[][] data) {
		// Default method
		if (this.method == null)
			this.method = new MutualInformation();

		this.method.setMatrix(matrix);
		double[] sim = new double[data.length];
		int i = 0;
		for (int[] is : data) {
			sim[i++] = method.similarity(is[0], is[1]);
		}
		return sim;
	}

	public double[] similarity(double[][] profile, int[][] data, DoubleFunction transformer) {
		return similarity(transform(profile, transformer), data);
	}

	public double[][] transform(double[][] matrix, DoubleFunction transformer) {
		return new DenseDoubleMatrix2D(matrix).assign(transformer).toArray();
	}

	/**
	 * Calculates AUC
	 * 
	 * @param y
	 * @param sim
	 * @return
	 */
	public double auc(int[] y, double[] sim) {
		int totalPositive = 0;
		int totalNegative = 0;
		List<Pair<Double, Integer>> sortedProb = new ArrayList<Pair<Double, Integer>>();
		for (int i = 0; i < y.length; i++) {
			if (Double.isNaN(sim[i]) || Double.isInfinite(sim[i])) continue;
			sortedProb.add(new Pair<Double, Integer>(sim[i], y[i]));
			if (y[i] == 0) {
				totalNegative++;
			} else {
				totalPositive++;
			}
		}
		Collections.sort(sortedProb, new Comparator<Pair<Double, Integer>>() {
			@Override
			public int compare(Pair<Double, Integer> o1, Pair<Double, Integer> o2) {
				return -o1.getFirst().compareTo(o2.getFirst());
			}
		});

		double fp = 0;
		double tp = 0;
		double fpPrev = 0;
		double tpPrev = 0;
		double area = 0;
		double fPrev = Double.MIN_VALUE;

		int i = 0;
		while (i < sortedProb.size()) {
			Pair<Double, Integer> pair = sortedProb.get(i);
			double curF = pair.getFirst();
			if (curF != fPrev) {
				area += Math.abs(fp - fpPrev) * ((tp + tpPrev) / 2.0);
				fPrev = curF;
				fpPrev = fp;
				tpPrev = tp;
			}
			double label = pair.getSecond();
			if (label == +1) {
				tp++;
			} else {
				fp++;
			}
			i++;
		}
		area += Math.abs(totalNegative - fpPrev) * ((totalPositive + tpPrev) / 2.0);
		area /= ((double) totalPositive * totalNegative);
		return area;
	}
}
