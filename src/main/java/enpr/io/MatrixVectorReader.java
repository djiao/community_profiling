/*
 Copyright (c) 2012, Dazhi Jiao 
 All rights reserved. 

 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
 this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
 notice, this list of conditions and the following disclaimer in the 
 documentation and/or other materials provided with the distribution. 
 * Neither the name of  nor the names of its contributors may be used to 
 endorse or promote products derived from this software without specific 
 prior written permission. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 POSSIBILITY OF SUCH DAMAGE. 
 */
package enpr.io;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author djiao
 */
public class MatrixVectorReader {
	private Reader reader = null;
	private char separator = '\t';

    public MatrixVectorReader(Reader reader, char separator) {
        this.reader = reader; 
        this.separator = separator;
    }

    public MatrixVectorReader(Reader reader) {
        this.reader = reader;
    }
    
    /**
     * Read a matrix with column and row names
     * 
     * @param colNames
     * @param rowNames
     * @return
     * @throws IOException
     */
    public double[][] readDoubleMatrix2D(List<String> colNames, List<String> rowNames) throws IOException {
    	Scanner in = new Scanner(reader);
        String [] header = in.nextLine().split(Character.toString(separator));
        
        if (!header[0].isEmpty()) {
        	colNames.add(header[0]);
        }
        for (int n = 1; n < header.length; n++) { 
        	colNames.add(header[n]);
        }

        String[] nextLine;
        List<double[]> data = new ArrayList<double[]>();
        while (in.hasNext()) {
        	String str = in.nextLine();
        	if (str.isEmpty()) {
        		continue;
        	}
        	nextLine = str.split(Character.toString(separator));
            String name = nextLine[0];
            rowNames.add(name);
            double[] line = new double[colNames.size()];
            for (int n = 0; n < colNames.size(); n++) {
                line[n] = Double.parseDouble(nextLine[n+1]);
            }
            data.add(line);
        } 
        in.close();
        return data.toArray(new double[data.size()][]);
    }
    
    /**
     * Read a matrix with no column and row names
     * 
     * @return
     * @throws IOException
     */
    public double[][] readDoubleMatrix2D() throws IOException {
		Scanner in = new Scanner(reader);
        String[] nextLine;
        List<double[]> d = new ArrayList<double[]>();
        while (in.hasNext()) {
        	nextLine = in.nextLine().split(Character.toString(separator));
            double[] line = new double[nextLine.length];
            for (int i = 0; i < nextLine.length; i++) {
                line[i] = Double.parseDouble(nextLine[i]);
            }
            d.add(line);
        } 
        in.close();
        return d.toArray(new double[d.size()][]);	    	
    }
    
    public void close() throws IOException {
    	if (reader != null) reader.close();
    }
}
