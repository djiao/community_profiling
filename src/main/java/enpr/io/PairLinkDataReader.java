package enpr.io;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.apache.mahout.common.Pair;

public class PairLinkDataReader {
	public Pair<int[][], int[]> readData(InputStream file, String[] names) throws IOException {
		List<int[]> list = new ArrayList<int[]>();
		List<Integer> yl = new ArrayList<Integer>();
		Scanner in = new Scanner(file);
		in.nextLine(); // skip header 
		List<String> nameList = Arrays.asList(names);
		while (in.hasNext()) {
			String[] line = in.nextLine().split("\t");
			int[] row = new int[2];
			row[0] = nameList.indexOf(line[0]);
			row[1] = nameList.indexOf(line[1]);
			list.add(row);
			yl.add(Integer.parseInt(line[2]));
		}
		int[][] data = list.toArray(new int[list.size()][]);
		int[] y = new int[yl.size()];
		for (int i = 0; i < yl.size(); i++) {
			y[i] = yl.get(i);
		}	
		return new Pair<int[][], int[]>(data, y);
	}
}
