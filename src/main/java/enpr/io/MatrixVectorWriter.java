/*
 Copyright (c) 2012, Dazhi Jiao 
 All rights reserved. 

 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
 this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
 notice, this list of conditions and the following disclaimer in the 
 documentation and/or other materials provided with the distribution. 
 * Neither the name of  nor the names of its contributors may be used to 
 endorse or promote products derived from this software without specific 
 prior written permission. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 POSSIBILITY OF SUCH DAMAGE. 
 */
package enpr.io;

import cern.colt.list.DoubleArrayList;
import cern.colt.list.IntArrayList;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author djiao
 */
public class MatrixVectorWriter extends PrintWriter {

    /**
     * Constructor for MatrixVectorWriter
     * 
     * @param out
     */
    public MatrixVectorWriter(OutputStream out) {
        super(out);
    }

    /**
     * Constructor for MatrixVectorWriter
     * 
     * @param out
     * @param autoFlush
     */
    public MatrixVectorWriter(OutputStream out, boolean autoFlush) {
        super(out, autoFlush);
    }

    /**
     * Constructor for MatrixVectorWriter
     * 
     * @param out
     */
    public MatrixVectorWriter(Writer out) {
        super(out);
    }

    /**
     * Constructor for MatrixVectorWriter
     * 
     * @param out
     * @param autoFlush
     */
    public MatrixVectorWriter(Writer out, boolean autoFlush) {
        super(out, autoFlush);
    }
    
    public void printMatrix(DoubleMatrix2D matrix) throws IOException {
        for (int i = 0; i < matrix.rows(); i++) {
            for (int j = 0; j < matrix.columns(); j++) {
                if (j < matrix.columns()-1) format("%12e\t", matrix.getQuick(i, j));
                else format("%12e\n", matrix.getQuick(i, j));
            }
        }
        flush();
    }
    
    public void printMatrix(DoubleMatrix2D matrix, List<String> rowNames, List<String> colNames) throws IOException {
        print("\t" + StringUtils.join(colNames, "\t") + "\n");
        for (int i = 0; i < matrix.rows(); i++) {
            print(rowNames.get(i) + "\t");
            for (int j = 0; j < matrix.columns(); j++) {
                if (j < matrix.columns()-1) format("%12e\t", matrix.getQuick(i, j));
                else format("%12e\n", matrix.getQuick(i, j));
            }
        }
        flush();
    }    
    
    /**
     * Writes a sparse matrix (SparseDoubleMatrix2D) in the following format: 
     * i   j   x
     * i,j: integer vectors of the same length specifying the locations (row and column indices) 
     *   of the non-zero (or non-TRUE) entries of the matrix.s is the starting node, 
     * x: values of the matrix entries
     * @param matrix
     * @param rowNames
     * @param colNames
     * @throws IOException 
     */
    public void printSparseMatrix(DoubleMatrix2D matrix, List<String> rowNames, List<String> colNames) throws IOException {
        IntArrayList rowList = new IntArrayList();
        IntArrayList colList = new IntArrayList();
        DoubleArrayList objList = new DoubleArrayList();
        matrix.getNonZeros(rowList, colList, objList);
        for (int i = 0; i < rowList.size(); i++) {
            format("%s\t%s\t%12e\n", rowNames.get(rowList.get(i)), colNames.get(colList.get(i)), objList.get(i));
        }
        flush();
    }    
    
    public void printSparseMatrix(DoubleMatrix2D matrix) throws IOException {
        IntArrayList rowList = new IntArrayList();
        IntArrayList colList = new IntArrayList();
        DoubleArrayList objList = new DoubleArrayList();
        matrix.getNonZeros(rowList, colList, objList);
        for (int i = 0; i < rowList.size(); i++) {
            format("%d\t%d\t%12e\n", rowList.get(i), colList.get(i), objList.get(i));
        }
        flush();
    }  
    
    public void printVector(DoubleMatrix1D vector) throws IOException {
        for (int i = 0; i < vector.size(); i++) {
            format("%12e\n", vector.get(i));
        }
        flush();
    }
    
    public void printVector(DoubleMatrix1D vector, List<String> names) throws IOException {
        for (int i = 0; i < vector.size(); i++) {
            format("%s\t%12e\n", names.get(i), vector.get(i));
        }
        flush();
    }    
    
    public void printVector(List<Object> vector, List<String> names) throws IOException {
        for (int i = 0; i < vector.size(); i++) {
            format("%s\t%s\n", names.get(i), vector.get(i));
        }
        flush();        
    }
}
