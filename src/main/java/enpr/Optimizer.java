package enpr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PatternOptionBuilder;
import org.apache.commons.cli.PosixParser;
import org.apache.mahout.common.Pair;

import cern.colt.function.DoubleFunction;
import enpr.io.MatrixVectorReader;
import enpr.io.PairLinkDataReader;
import enpr.io.newick.NewickParser;
import enpr.optimizer.TaxonSetOptimizer;
import enpr.optimizer.TreeBasedSearch;
import enpr.similarity.SimilarityMeasure;
import enpr.transform.BinaryReverse;
import enpr.tree.Tree;

/**
 * Optimize by select a subset of samples in profile
 * 
 * @author djiao
 */
public class Optimizer {
	private static final Logger logger = Logger.getLogger(Optimizer.class.getName());

	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CommandLineParser parser = new PosixParser();
		File profFile = null;
		File dataFile = null;
		File outFile = null;
		File treeFile = null;
		DoubleFunction transformer = null;
		double cutoff = 1e-5;
		Class<DoubleFunction> transformerClass = null;
		TaxonSetOptimizer optimizer = null;
		Class<TaxonSetOptimizer> optimizerClass = null;
		Class<SimilarityMeasure> similarityClass = null;
		
		Options options = new Options();
		options.addOption(App.buildOption("h", "help",        false, "print help", false, null));
		options.addOption(App.buildOption("p", "profile",     true, "profile matrix (required)", true, PatternOptionBuilder.EXISTING_FILE_VALUE));
		options.addOption(App.buildOption("d", "datafile",    true, "pair wise link data file (required)", true, PatternOptionBuilder.EXISTING_FILE_VALUE));
		options.addOption(App.buildOption("r", "transformer", true, "value transformer (optional, default null)", false, PatternOptionBuilder.CLASS_VALUE));
		options.addOption(App.buildOption("c", "cutoff",      true, "cutoff value if transformer is enpr.transform.BinaryReverse. (optional, default 1e-5)", false, PatternOptionBuilder.NUMBER_VALUE));
		options.addOption(App.buildOption("o", "output",      true, "output file. optimized set will be written (required)", true, PatternOptionBuilder.FILE_VALUE));
		options.addOption(App.buildOption("t", "tree-file",   true, "tree file (required if optimizer is tree based)", false, PatternOptionBuilder.EXISTING_FILE_VALUE));
		options.addOption(App.buildOption("m", "optimizer",   true, "optimizer class (optional, default enpr.optimizer.TreeBasedSearch", false, PatternOptionBuilder.CLASS_VALUE));
		options.addOption(App.buildOption("s", "similarity",  true, "similarity measure class (optional, default enpr.similarity.MutualInformation", false, PatternOptionBuilder.CLASS_VALUE));
		
		try {
			CommandLine line = parser.parse(options, args);
			
			if (line.hasOption('h')) {
				printHelp(options, TreeBuilder.class.getName());
				System.exit(0);
			}
			profFile = (File) line.getParsedOptionValue("p");
			outFile = (File)line.getParsedOptionValue("o");
			dataFile = (File)line.getParsedOptionValue("d");
			if (line.hasOption('t')) {
				treeFile = (File)line.getParsedOptionValue("t");
			}
			
			if (line.hasOption('r')) {
				if (line.hasOption('c')) {
					cutoff = ((Number)line.getParsedOptionValue("c")).doubleValue();
				}
				transformerClass = (Class<DoubleFunction>)line.getParsedOptionValue("r");
				if (transformerClass.getName().equals(BinaryReverse.class.getName())) {
					transformer = new BinaryReverse(cutoff);
				} else {
					transformer = (DoubleFunction)transformerClass.newInstance();
				}
			}
			if (line.hasOption('m')) {
				optimizerClass = (Class<TaxonSetOptimizer>)line.getParsedOptionValue("m");
			}
			if (line.hasOption('s')) {
				similarityClass = (Class<SimilarityMeasure>)line.getParsedOptionValue("s");
			}
			
		} catch (ParseException ex) {
			logger.log(Level.SEVERE, "Unexpected exception: {0}", ex.getMessage());
			printHelp(options, TreeBuilder.class.getName());
			System.exit(1);
		} catch (InstantiationException e) {
			logger.log(Level.SEVERE, "Can't start transformer class {0}: {1}", new Object[]{transformerClass.getName(), e.getMessage()});
			System.exit(1);
		} catch (IllegalAccessException e) {
			logger.log(Level.SEVERE, "Can't start transformer class {0}: {1}", new Object[]{transformerClass.getName(), e.getMessage()});
			System.exit(1);
		} 
		
		MatrixVectorReader reader;
		Profile profile = new Profile();
		List<String> colNames = new ArrayList<>();
		List<String> rowNames = new ArrayList<>();
		Tree tree;
		try {
			if (profFile.getName().endsWith("gz")) {
				reader = new MatrixVectorReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(profFile))));
			} else if (profFile.getName().endsWith("zip")) {
				reader = new MatrixVectorReader(new InputStreamReader(new ZipInputStream(new FileInputStream(profFile))));
			} else {
				reader = new MatrixVectorReader(new FileReader(profFile));
			}
			double[][] matrix = reader.readDoubleMatrix2D(colNames, rowNames);
			profile.setRowNames(rowNames.toArray(new String[rowNames.size()]));
			profile.setColNames(colNames.toArray(new String[colNames.size()]));
			profile.setProfile(matrix);	
			if (transformer != null) {
				profile.setProfile(profile.getProfile().assign(transformer));
			}
			
			if (treeFile != null) {
				tree = new NewickParser(new FileInputStream(treeFile)).tree();
				profile.setTree(tree);
			}
			
			Pair<int[][], int[]> pair;
			PairLinkDataReader linkReader = new PairLinkDataReader();
			if (dataFile.getName().endsWith("gz")) {
				pair = linkReader.readData(new GZIPInputStream(new FileInputStream(dataFile)), profile.getRowNames());
			} else if (dataFile.getName().endsWith("zip")) {
				pair = linkReader.readData(new ZipInputStream(new FileInputStream(dataFile)), profile.getRowNames());
			} else {
				pair = linkReader.readData(new FileInputStream(dataFile), profile.getRowNames());
			}		
			
			Predictor predictor = new Predictor();
			if (similarityClass != null) {
				predictor.setMethod(similarityClass.newInstance());
			}
			
			if (optimizerClass != null) {
				Constructor<?> ctor = optimizerClass.getConstructor(Predictor.class, Profile.class, int[][].class, int[].class);
				optimizer = (TaxonSetOptimizer)ctor.newInstance(new Object[] {predictor, profile, pair.getFirst(), pair.getSecond()});
			} else {
				optimizer = new TreeBasedSearch(predictor, profile, pair.getFirst(), pair.getSecond());
			}
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Error in reading file {0}", ex.getMessage());
			ex.printStackTrace();
			System.exit(1);
		} catch (enpr.io.newick.ParseException e) {
			logger.log(Level.SEVERE, "Can't parse tree file {0}: {1}", new Object[] { treeFile.getAbsolutePath(), e.getMessage()});
			e.printStackTrace();
			System.exit(1);
		} catch (InstantiationException e) {
			logger.log(Level.SEVERE, "Can't start class {0}", e.getMessage());
			e.printStackTrace();
			System.exit(1);
		} catch (IllegalAccessException e) {
			logger.log(Level.SEVERE, "Can't start class {0}", e.getMessage());
			e.printStackTrace();
			System.exit(1);
		} catch (SecurityException e) {
			logger.log(Level.SEVERE, "Can't start class {0}", e.getMessage());
			e.printStackTrace();
			System.exit(1);
		} catch (NoSuchMethodException e) {
			logger.log(Level.SEVERE, "Can't start class {0}", e.getMessage());
			e.printStackTrace();
			System.exit(1);
		} catch (IllegalArgumentException e) {
			logger.log(Level.SEVERE, "Can't start class {0}", e.getMessage());
			e.printStackTrace();
			System.exit(1);
		} catch (InvocationTargetException e) {
			logger.log(Level.SEVERE, "Can't start class {0}", e.getMessage());
			e.printStackTrace();
			System.exit(1);
		} 
		
		boolean[] result = optimizer.optimize();
		int nTrue = 0;
		try {
			PrintWriter pw = new PrintWriter(new FileWriter(outFile));
			for (int i = 0; i < result.length; i++) {
				if (result[i]) {
					pw.write(colNames.get(i) + "\n");
					nTrue++;
				}
			}
			pw.close();
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Error in writing file {0}: {1}", new Object[] { outFile.getAbsolutePath(), ex.getMessage()});
			System.exit(1);
		}
		System.out.println("AUC after optimization: " + optimizer.getAuc());
		System.out.println("Number of taxon after/before optimization: " + nTrue + "/" + result.length);
	}
	
	public static void printHelp(Options options, String cls) {
		HelpFormatter hf = new HelpFormatter();
		hf.printHelp("java " + cls, options);
	}

}
