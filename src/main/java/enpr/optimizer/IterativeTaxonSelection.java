package enpr.optimizer;

import java.util.Random;

import cern.colt.GenericPermuting;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import enpr.Predictor;
import enpr.Profile;

public class IterativeTaxonSelection extends TaxonSetOptimizer {

	public IterativeTaxonSelection(Predictor predictor, Profile profile, int[][] data, int[] y) {
		super(predictor, profile, data, y);
	}

	@Override
	public boolean[] optimize() {
		init();
		while (true) {
			double currentAuc = auc;
			randomOptimize();
			if (currentAuc <= auc) {
				break;
			}
		}
		boolean[] solution = new boolean[currentSolution.size()];
		for (int i = 0; i < currentSolution.size(); i++) {
			solution[i] = currentSolution.getQuick(i) == 1.0;
		}
		return solution;		
	}
	
	public void init() {
		DoubleMatrix1D solution = new DenseDoubleMatrix1D(profile.getProfile().columns());
		for (int i = 0; i < solution.size(); i++) {
			solution.setQuick(i, new Random().nextBoolean() ? 1.0: 0.0);
		}
		currentSolution = solution;
		auc = auc(currentSolution);
	}
	
	public void randomOptimize() {
		DoubleMatrix1D solution = new DenseDoubleMatrix1D(currentSolution.size());
		int[] order = GenericPermuting.permutation(1, solution.size());
		for (int i : order) {
			solution.setQuick(i, 1.0-solution.getQuick(i));
			double auc_p = auc(solution);
			if (auc_p > auc) {
				auc = auc_p;
				currentSolution = solution;
			}
		}
	}
}
