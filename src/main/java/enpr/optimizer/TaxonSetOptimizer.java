package enpr.optimizer;

import cern.colt.list.DoubleArrayList;
import cern.colt.list.IntArrayList;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import enpr.Predictor;
import enpr.Profile;
import enpr.tree.Tree;

public abstract class TaxonSetOptimizer {
	protected double auc;
	protected DoubleMatrix1D currentSolution;
	protected Predictor predictor;	
	protected Profile profile;
	protected int[][] data;
	protected int[] y;
	
	public TaxonSetOptimizer(Predictor predictor, Profile profile, int[][] data, int[] y) {
		this.predictor = predictor;
		this.profile = profile;
		this.data = data;
		this.y = y;
	}	
	
	public TaxonSetOptimizer(Predictor predictor, double[][] matrix, int[][] data, int[] y) {
		this.predictor = predictor;
		this.profile = new Profile();
		profile.setProfile(matrix);
		this.data = data;
		this.y = y;		
	}
	
	public TaxonSetOptimizer(Predictor predictor, double[][] matrix, int[][] data, int[] y, Tree tree) {
		this.predictor = predictor;
		this.profile = new Profile();
		this.profile.setTree(tree);
		profile.setProfile(matrix);
		this.data = data;
		this.y = y;		
	}	
	
	/**
	 * @return the currentSolution
	 */
	public DoubleMatrix1D getCurrentSolution() {
		return currentSolution;
	}
	
	public int[] getSolution() {
		int[] solution = new int[currentSolution.size()];
		for (int i = 0; i < currentSolution.size(); i++) {
			solution[i] = (int)currentSolution.getQuick(i);
		}
		return solution;
	}

	/**
	 * @return the auc
	 */
	public double getAuc() {
		return auc;
	}

	
	/**
	 * Calculate AUC based on a taxon set
	 * 
	 * @return
	 */
	public double auc(DoubleMatrix1D solution) {
		return predictor.auc(y, similarity(solution));
	}
	
	/**
	 * Returns a value based on a solution
	 * 
	 * @param solution
	 * @return
	 */
	public double[] similarity(DoubleMatrix1D solution) {
		DoubleMatrix2D matrix = profile.getProfile();
		IntArrayList idx = new IntArrayList(solution.size());
		solution.getNonZeros(idx, new DoubleArrayList());
		int[] all = new int[matrix.rows()];
		for (int i = 0; i < matrix.rows(); i++) all[i] = i;
		DoubleMatrix2D submatrix = matrix.viewSelection(all, idx.elements());
		return predictor.similarity(submatrix.toArray(), data);			
	}
	
	public abstract boolean[] optimize();
}
