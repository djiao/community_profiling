package enpr.optimizer;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import enpr.Predictor;
import enpr.Profile;
import enpr.tree.Tree;
import enpr.tree.TreeNode;

public class TreeBasedSearch extends TaxonSetOptimizer {
	private final static Logger log = Logger.getLogger(TreeBasedSearch.class.getName());
	
	private final Tree tree;
	/**
	 * Nodes that can improve AUC
	 */
	private Map<TreeNode,Double> impNodes;
	private Set<TreeNode> currentNodes;
	private Map<TreeNode, Integer> levels = new HashMap<TreeNode, Integer>();
	
	public int setLevel(TreeNode node) {
		if (node.isLeaf()) {
			levels.put(node,  tree.getHeight());
			return tree.getHeight();
		}
		int level = Integer.MAX_VALUE;
		for (TreeNode child: node.getChildren()) {
			level = Math.min(level, setLevel(child)-1);
		}		
		levels.put(node, level);
		return level;
	}
	
	public TreeBasedSearch(Predictor predictor, Profile profile, int[][] data, int[] y) {
		super(predictor, profile, data, y);
		this.tree = profile.getTree();
		
		if (null == profile.getColNames() || profile.getColNames().length == 0) {
			log.log(Level.WARNING, "Profile can't map tree leaf nodes to column names because column names are empty");
		} else {
			tree.mapLeafKeys(Arrays.asList(profile.getColNames()));	
		}		
		this.tree.trimTree();
		// Set levels in tree
		setLevel(tree.getRoot());
	}

	private boolean remove(TreeNode node) {
		DoubleMatrix1D solution = new DenseDoubleMatrix1D(currentSolution.toArray());
		for (TreeNode leaf: node.getLeaves())
			solution.setQuick(leaf.getLeafKey(), 0.0);
		if (solution.cardinality() == 0) return false;  // Can't remove everything
		//System.out.println("Evaluating solution with " + solution.cardinality() + " taxa");
		double auc_p = auc(solution);
		if (auc_p > auc) {
			double d_auc = auc_p - auc;
			auc = auc_p;
			impNodes.put(node, d_auc);
			currentSolution = solution;
			System.out.println("AUC: " + auc + " [" + currentSolution.cardinality() + "]");
			return true;
		} else {
			return false;
		}
	}	
	
	private void flip(TreeNode node) {
		DoubleMatrix1D solution = new DenseDoubleMatrix1D(currentSolution.toArray());
		for (TreeNode leaf: node.getLeaves()) {
			double current = solution.getQuick(leaf.getLeafKey());
			solution.setQuick(leaf.getLeafKey(), 1.0-current);  // flip it
		}
		//System.out.println("Evaluating solution with " + solution.cardinality() + " taxa");
		double auc_p = auc(solution);
		double d_auc = auc_p - auc;
		if (auc_p > auc) {
			auc = auc_p;
			currentSolution = solution;
		}
		impNodes.put(node, Math.abs(d_auc));
	}

	public void step1() {
		this.currentSolution = new DenseDoubleMatrix1D(tree.getRoot().getNumberLeaves());
		currentSolution.assign(1.0);
		auc = this.auc(currentSolution);

		this.impNodes = new HashMap<TreeNode, Double>();
		this.currentNodes = new HashSet<TreeNode>();
		TreeNode root = tree.getRoot();
		currentNodes.add(root);
		
		for (int l = 1; l <= tree.getHeight(); l++) {
			System.out.println("Tree Nodes at level " + l);
			Set<TreeNode> nodes = new HashSet<TreeNode>();
			for (TreeNode node: currentNodes) {
				for (TreeNode child: node.getChildren()) {
					if (levels.get(child) < l) {  // already checked
						continue;
					} else if (levels.get(child) > l) {  // not yet
						nodes.add(node);
					} else { // check it 
						if (!child.isLeaf()) {
							boolean remove = remove(child);
							if (!remove) nodes.add(child);
						}
					}
				}
			}
			this.currentNodes = nodes;
		}
		
		System.out.println("remove: " + impNodes.size() + "; auc=" + this.auc);
	}
	
	public void step2() {
		while (impNodes.size() > 0) {
			Map.Entry<TreeNode, Double> pair = Collections.min(impNodes.entrySet(), new Comparator<Map.Entry<TreeNode, Double>>() {
				@Override
				public int compare(Map.Entry<TreeNode, Double> o1, Map.Entry<TreeNode, Double> o2) {
					return o1.getValue().compareTo(o2.getValue());
				}
			});
			TreeNode node = pair.getKey();
			for (TreeNode child: node.getChildren()) {
				flip(child);
			}
			impNodes.remove(pair.getKey()); // remove current node
		}
	}
	
	public void step3() {
		for (int i = 0; i < currentSolution.size(); i++) {
			if (currentSolution.getQuick(i) == 1.0) {
				currentSolution.setQuick(i, 0.0);
				double auc_p = auc(currentSolution);
				if (auc_p > auc) {
					auc = auc_p;
				} else {
					currentSolution.setQuick(i, 1.0);
				}				
			}
		}
	}

	@Override
	public boolean[] optimize() {
		step1();
		step2();
		step3();
		
		boolean[] solution = new boolean[currentSolution.size()];
		for (int i = 0; i < currentSolution.size(); i++) {
			solution[i] = currentSolution.getQuick(i) == 1.0;
		}
		return solution;
	}
	

}
