package enpr.optimizer;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.uncommons.maths.binary.BitString;
import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.CandidateFactory;
import org.uncommons.watchmaker.framework.EvolutionObserver;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.GenerationalEvolutionEngine;
import org.uncommons.watchmaker.framework.PopulationData;
import org.uncommons.watchmaker.framework.SelectionStrategy;
import org.uncommons.watchmaker.framework.factories.BitStringFactory;
import org.uncommons.watchmaker.framework.operators.BitStringCrossover;
import org.uncommons.watchmaker.framework.operators.BitStringMutation;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.termination.Stagnation;

import cern.colt.list.DoubleArrayList;
import cern.colt.list.IntArrayList;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;

import com.rits.cloning.Cloner;

import enpr.Predictor;
import enpr.Profile;

public class GeneticAlgorithmOptimizer extends TaxonSetOptimizer {
	public GeneticAlgorithmOptimizer(Predictor predictor, Profile profile, int[][] data, int[] y) {
		super(predictor, profile, data, y);
	}
	
	@Override
	public double auc(DoubleMatrix1D solution) {
		DoubleMatrix2D matrix = profile.getProfile();
		IntArrayList idx = new IntArrayList(solution.size());
		solution.getNonZeros(idx, new DoubleArrayList());
		int[] all = new int[matrix.rows()];
		for (int i = 0; i < matrix.rows(); i++) all[i] = i;
		DoubleMatrix2D submatrix = matrix.viewSelection(all, idx.elements());
		Cloner cloner = new Cloner();
		Predictor clone = cloner.deepClone(predictor);
		double[] values = clone.similarity(submatrix.toArray(), data);
		return predictor.auc(y, values);				

	}

	@Override
	public boolean[] optimize() {
		CandidateFactory<BitString> factory = new BitStringFactory(profile.getProfile().columns());
		List<EvolutionaryOperator<BitString>> operators = new LinkedList<EvolutionaryOperator<BitString>>();
		operators.add(new BitStringMutation(new Probability(0.02)));
		operators.add(new BitStringCrossover());
		EvolutionaryOperator<BitString> pipeline = new EvolutionPipeline<BitString>(operators);

		FitnessEvaluator<BitString> fitnessEvaluator = new FitnessEvaluator<BitString>() {

			@Override
			public double getFitness(BitString arg0, List<? extends BitString> arg1) {
				DoubleMatrix1D solution = new DenseDoubleMatrix1D(arg0.getLength());
				for (int i = 0; i < solution.size(); i++) {
					solution.setQuick(i, arg0.getBit(i) ? 1.0 : 0.0);
				}
				return auc(solution);
			}

			@Override
			public boolean isNatural() {
				return true;
			}
		};
		SelectionStrategy<Object> selection = new RouletteWheelSelection();
		Random rng = new MersenneTwisterRNG();
		
		GenerationalEvolutionEngine<BitString> engine = new GenerationalEvolutionEngine<BitString>(factory, pipeline, fitnessEvaluator,
				selection, rng);
		engine.setSingleThreaded(false); // ensure running in multi-threaded mode. 
		
		engine.addEvolutionObserver(new EvolutionObserver<BitString>() {
			public void populationUpdate(PopulationData<? extends BitString> data) {
				System.out.printf("Generation %d: %s\n", data.getGenerationNumber(), data.getBestCandidateFitness());
			}
		});		
		
		System.out.println("Start evolving");
		BitString result = engine.evolve(50, 5, new Stagnation(100, true));
		//System.out.println(result);
		boolean[] ret = new boolean[result.getLength()];
		for (int i = 0; i < result.getLength(); i++) {
			ret[i]= result.getBit(i);
		}
		return ret;
	}
}


